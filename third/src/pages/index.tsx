import styles from './index.less'; // 启用css-module
import Route from "@/component/Route"
import Item from "@/component/Item"
import InfiniteScroll from 'react-infinite-scroll-component';
import { Carousel } from "antd"
import { IRootState } from "@/types";
import React, { useEffect, useMemo, useState } from "react"
import { NavLink, useDispatch, useSelector } from "umi";
import RecommendedReading from "@/component/RecommendedReading"
import Lable from "@/component/lable"

function IndexPage() {
  let [page, setPage] = useState(1);
  const state = useSelector((state: IRootState) => ({ ...state.article, ...state.loading.effects }))
  const dispatch = useDispatch()
  // 请求文章列表数据
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page,
    });
  }, [page]);
  // 请求推荐阅读数据和文章标题
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    });
    dispatch({
      type: "article/getArticlelabel"
    })
  }, [])
<<<<<<< HEAD

  // 页数加1
  function pullupLoader() {
    setPage(page => page + 1)
  }
=======

  // 页数加1
  function pullupLoader() {
    setPage(page => page + 1)
  }

  //刷统计量
  // useEffect(()=>{
  //   setTimeout(()=>{
  //     window.location.href='https://jasonandjay.com/1812A/luyanyan/1/'
  //   },1000)
  // },[])
>>>>>>> 1592feb3cc95591b1f4b918c7beb8b86851e52ae

  return <div className={styles.container}>
    <div className={styles.context}>
      <div className={styles.left}>
        {/* 轮播图 */}
        <div className={styles.swiper}>
          <div className={styles.img}>
            <Carousel className={styles.imgbox} autoplay>
              <div className={styles.img1}>
                <div className={styles.dialog1}>
                  <NavLink to="/archive/f92ffbce-5945-4e7a-9f66-28aaee7dc0ae" className={styles.dialogcontext1}>
                    <h2>互联网工作原理</h2>
                    <p>
                      <span><time dateTime="2021-05-13 03:36:07">3 个月前</time></span>
                      <span className="_2Ntia1kMYOAiuHgvb4eUCM">·</span>
                      <span>1832 次阅读</span>
                    </p>
                  </NavLink>
                  <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-05-13/internet-emerge-econ_1200x675_hero_071317.jpeg" alt="" />
                </div>
              </div>
              <div className={styles.img2}>
                <div className={styles.dialog2}>
                  <NavLink to="/archive/db67efc4-3020-4ba9-95bb-8bb96a198b4a" className={styles.dialogcontext2}>
                    <h2>计算机工作原理</h2>
                    <p>
                      <span><time dateTime="2021-05-13 09:06:41">3 个月前</time></span>
                      <span className="_2Ntia1kMYOAiuHgvb4eUCM">·</span>
                      <span>2120 次阅读</span>
                    </p>
                  </NavLink >
                  <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-05-13/telework-5059653_1280.webp" alt="" />
                </div>
              </div>
            </Carousel>
          </div>
        </div>
        {/* 路由切换 */}
        <Route></Route>
        {/* 无限滚动 */}
        <div className={styles.context} style={{ overflow: "auto" }}>
          <InfiniteScroll
            hasMore={state.articleCount > page * 12}
            scrollableTarget="scrollableDiv"
            loader={<h4>Loading...</h4>}
            dataLength={state.articleList.length}
            next={pullupLoader}
          >{
              state.articleList.map(item => {
                return <Item key={item.id} item={item}></Item>
              })
            }</InfiniteScroll>
        </div>
      </div>
      <div className={styles.right}>
        {/* 推荐阅读 */}
        <RecommendedReading bol={state["article/getRecommend"] && state.recommend.length === 0} recommend={state.recommend}></RecommendedReading>
        {/* 文章标签 */}
        <Lable
          bol={state["article/getArticlelabel"] && state.articlelableList.length === 0}
          articlelableList={state.articlelableList}
        ></Lable>
      </div>
    </div>
  </div>

}
export default IndexPage