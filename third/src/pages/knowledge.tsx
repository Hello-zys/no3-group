import React from 'react';
import { useEffect } from 'react'
import { useDispatch, useHistory, useSelector } from 'umi'
import { IArchiveStateItem, IknowState, IRootState } from "@/types"
import RecommendedReading from "@/component/RecommendedReading"   //===组件
import { ICategoryItem } from "@/types/knowledge";
import Categories from "@/component/ArticleClassification"
import {RouteComponentProps} from "react-router-dom"
import styles from './knowledge.less';  // from启用css-module
import classNames from "classnames"
import Item from '@/component/Item';

// interface Props {
//   category: ICategoryItem[]
// }
const knowledge: React.FC<RouteComponentProps> = (props) => {
  const dispatch = useDispatch()
  const history = useHistory()
  const state = useSelector((state: IknowState) => ({...state.knowledge,...state.loading.effects}))
  const stat = useSelector((state: IRootState) => ({...state.article,...state.loading.effects}));

  useEffect(() => {
    dispatch({
      type: "knowledge/getKnowledgeList"
    })
  }, [])
  useEffect(() => {
    // console.log(category,"****category**");
    dispatch({
      type: "knowledge/getCategoryList"
    })
  }, [])
  return (
    <div className={styles.knowledge}>
      <div className={styles.container}>
        <div className={styles.container_box}>
          <div className={classNames(styles.box_left, styles.width)}>
            <div className={styles.box_left_text}>
              {
                state.knowledge.map(item => {
                  return <div onClick={()=>history.push(`/knowledge/${item.id}`)} key={item.id}>
                    <Item item={item} />
                  </div>
                })
              }
            </div>
          </div>
          <div className={styles.box_right}>
            {/* 推荐阅读 */}
            {stat.recommend && <RecommendedReading recommend={stat.recommend} bol={stat["knowledge/getKnowledgeList"] && stat.recommend.length === 0}/>}
            {state.category && <Categories CategoryList={state.category} bol={state["knowledge/getCategoryList"] && state.category.length===0} callback={(id) => { props.history.push(`/category/${id}`) }}/>}
          </div>
        </div>
      </div>
    </div>
  )
}
export default knowledge