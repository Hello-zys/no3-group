import { IknowledgeItem, IknowState } from '@/types'
import Moment from '@/utils/moment'
import classNames from 'classnames'
import React, { useEffect } from 'react'
import { IRouteComponentProps, useDispatch, useSelector } from 'umi'
import Image from '@/component/Image';
import styles from "./detail.less"
import share from '@/component/Sharing'

const knowledgeDetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  let id = props.match.params.id
  const dispatch = useDispatch()
  const { detailItem, knowledge } = useSelector((state: IknowState) => state.knowledge)

  useEffect(() => {
    dispatch({
      type: "knowledge/getKnowledgeDetail",
      payload: id
    })
  }, [])
  const AA = (Id: string) => {
    dispatch({
      type: "knowledge/getKnowledgeDetail",
      payload: Id
    })
    props.match.params.id = Id
  }
  const ShareDetail=(e: React.MouseEvent,item: IknowledgeItem)=>{
      e.stopPropagation()
      share(item)
  }
  
  return (
    <div className={styles.detail}>
      <div className={styles.detail_top}>
        <div>
          <span><a href="/knowledge">知识小册</a></span>
          <span>/</span>
          <span>Web性能指南</span>
        </div>
      </div>
      <div className={styles.detail_bottom}>
        <div className={styles.detail_container}>
          <div className={classNames(styles.left, styles.width)} >
            <div className={styles.left_con}>
              <Image>
                <section>
                  <p className={styles.left_title}>{detailItem.title!}</p>
                  <div className={styles.left_img}>
                    <img src={detailItem.cover!} alt="" />
                  </div>
                  <div className={styles.left_content}>
                    <p className={styles.left_title}>{detailItem.title!}</p>
                    <p className={styles.span}>{detailItem.summary!}</p>
                    <p className={styles.spans}><span>{detailItem.views!}次阅读</span> <span>.</span> <span>{Moment(detailItem.publishAt).format("YYYY-MM-DD:HH:mm:ss")}</span></p>
                    <p><button><span onClick={() => {
                      props.history.push({
                        pathname: `/knowledge/${id}/${detailItem.children && detailItem.children[0].id}`
                      })
                    }}>开始阅读</span></button></p>
                  </div>
                </section>
              </Image>
              <ul className={styles.left_text}>
                {
                  detailItem.children?.map(item => {
                    return <li key={item.id} className={styles.TIT}>
                      <a href="">
                        <span onClick={() => {
                          props.history.push({
                            pathname: `/knowledge/${id}/${detailItem.children && detailItem.children[1].id}`
                          })
                        }}
                        >{item.title}</span>
                        <span>{Moment(detailItem.publishAt).format("YYYY-MM-DD:HH:mm:ss")}&gt;</span>
                      </a>
                    </li>
                  })
                }
              </ul>
            </div>
          </div>
          <div className={styles.right}>
            <div className={styles.right_con}>
              <p className={styles.Rtitle}>其他知识笔记</p>

              {knowledge.filter(item => id !== item.id).map(item => {
                return <li key={item.id} onClick={() => AA(item.id)}>
                  <p>
                    <span className={styles.title}>{item.title}</span>
                    <span>{Moment(item.createAt, "YYYYMMDD").fromNow()}</span>
                  </p>
                  <div className={styles.context_text}>
                    <div className={styles.img}>
                      <img src={item.cover} alt="" />
                    </div>
                    <div className={styles.text}>
                      <p className={styles.text_title}>{item.summary}</p>
                      <p>
                        <span>{item.views}</span>
                        <span onClick={(e)=>{ShareDetail(e,item)}}>分享</span>
                      </p>
                    </div>
                  </div>
                </li>
              })
              }
            </div>

          </div>

        </div>
      </div>
    </div>
  )
}
export default knowledgeDetail