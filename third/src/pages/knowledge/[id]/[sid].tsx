import { IknowState } from '@/types'
import React, { useCallback, useEffect, useMemo } from 'react'
import { IRouteComponentProps, useDispatch, useSelector } from 'umi'
import styles from "./sid.less"
import Popouts from "@/component/Popouts"
import Linkage from "@/component/Linkage";
import Like from "@/component/Like"

const knowledgeDetail2: React.FC<IRouteComponentProps<{ id: string, sid: string }>> = (props) => {
  let id = props.match.params.sid;
  const dispatch = useDispatch();
  const markdown = React.createRef<HTMLDivElement>()
  const { detailItem, startReading, toc } = useSelector((state: IknowState) => state.knowledge)
  useEffect(() => {
    dispatch({
      type: "knowledge/getStartReading",
      payload: id
    })
    dispatch({
      type: "knowledge/getKnowledgeDetail",
      payload: props.match.params.id
    })
  }, [id])

  function setSid(sid: string) {
    props.history.push({
      pathname: `/knowledge/${props.match.params.id}/${sid}`
    })
    dispatch({
      type: "knowledge/getStartReading",
      payload: sid
    })
  }


  let title = detailItem.children?.find(item => item.id === id)

  return <div className={styles.container}>
    <div className={styles.head}>
      <a href="/knowledge">知识小册</a>/
      <a href={`/knowledge/${props.match.params.id}`}>{detailItem.title}</a>/
      <span>{title?.title}</span>
    </div>
    <div className={styles.context}>
      <div className={styles.left}>
        <section className={styles.article}>
          <div className={styles.title}>
            <h1>{startReading.title}</h1>
            <p><i>发布于{startReading.createAt}<b>.</b>阅读量{startReading.views}</i></p>
          </div>
          <section className={styles.markdown} ref={markdown} dangerouslySetInnerHTML={{ __html: startReading.html! }}>
          </section>
          <p className={styles.publish}>发布于{startReading.publishAt}| 版权信息：非商用-署名-自由转载</p>
          <div className={styles.pinglun}>评论</div>
          <Popouts bol={useMemo(() => false, [false])} callback={useCallback(() => { }, [])} ></Popouts>
        </section>
      </div>
      <div className={styles.right}>
        <aside className={styles.aside}>
          <div className={styles.fingerpost}>
            <div className={styles.header}>{detailItem.title}</div>
            <div className={styles.main}>
              <ul>
                {
                  detailItem.children && detailItem.children.map(item => {
                    return <li key={item.id} className={item.id === id ? styles.active : ""} onClick={() => setSid(item.id)}>{item.title}</li>
                  })
                }
              </ul>
            </div>
          </div>
          <div className={styles.catalogue}>
            {toc && <Linkage toc={useMemo(() => toc, [toc])} />}
          </div>
        </aside>
      </div>
    </div>
    <Like id={useMemo(() => id, [id])}></Like>
  </div>
}
export default knowledgeDetail2