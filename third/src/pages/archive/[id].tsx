import { IArchiveStateItem, IRootState } from "@/types";
import React from "react";
import { useEffect } from "react";
import styles from "./index.less"
import { IRouteComponentProps, useDispatch, useSelector } from "umi"
import RecommendComponent from "@/component/RecommendedReading"
import HighLight from "@/component/Highlight";
import moment from "@/utils/moment"
import ImageScale from "../../component/Image/index"
import { useState } from "react";
import Linkage from "@/component/Linkage"
import Dialog from "@/component/Popouts"
import Like from "@/component/Like"
const ArchiveDetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  let id = props.match.params.id
  let articleId = props.match.params.id
  const dispatch = useDispatch()
  const state = useSelector((state: IArchiveStateItem) =>({...state.archive,...state.loading.effects}))
  let NewDeatilRecommendList =state.DeatilRecommendList.slice(0, 6) //详情推荐阅读
  let [visible, setvisible] = useState(false) //控制高亮
  let [hots, sethots] = useState(state.DetailItem.likes || 0);
  useEffect(()=>{
    sethots(state.DetailItem.likes)
  },[state.DetailItem])

  // 获取文章详情
  useEffect(() => {
    dispatch({
      type: "archive/getDeatil",
      payload: id
    })

  }, [id])
  // 详情推荐阅读
  useEffect(() => {
    dispatch({
      type: "archive/getDetailRecommend",
      payload: articleId
    })
  }, [id])
  return (
    <div className={styles.detail}>
      <div className={styles.container}>
          <section className={styles.left}>
            <div className={styles.leftTop}>
            <ImageScale>
              <div className={styles.imgs}>
                <img src={state.DetailItem.cover} alt="" />
              </div>
              <div className={styles.top}>
                <h1>{state.DetailItem.title}</h1>
                <p>
                  <span>
                    发布于
                      <time dateTime={moment(state.DetailItem.publishAt).format("YYYY--MM--DD HH:mm:ss")}>{moment(state.DetailItem.publishAt).format(" YYYY-MM-DD HH:mm:ss")}</time>
                  </span>
                  <span> . </span>
                  <span> 阅读量 {state.DetailItem.views}</span>
                </p>
              </div>
              <HighLight>
                <div dangerouslySetInnerHTML={{ __html: state.DetailItem.html! }}>
                </div>
              </HighLight>
            </ImageScale>
            </div>
              <div className={styles.Commenttitle}>
                  评论
              </div>
            <div className={styles.dialog}><Dialog bol={false} callback={()=>{}}></Dialog></div>
          </section>
          <div className={styles.right}>
            <RecommendComponent recommend={NewDeatilRecommendList} bol={state["archive/getDetailRecommend"]&&state.DeatilRecommendList.length===0}></RecommendComponent>
            <Linkage toc={state.toc}></Linkage>
          </div>
        
      </div>
        <Like id={state.DetailItem.id}/>
    </div>
  )
}
export default ArchiveDetail