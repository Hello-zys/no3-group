import React, { ReactElement, useEffect, useState } from 'react'
import style from './regards.less'
import { useDispatch } from 'umi';
interface Props {

}

export default function regards({ }: Props): ReactElement {
  const [page, setPage] = useState(1)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch({
      type: "regards/getregardsList",
      payload: {
        page,
        id: "8d8b6492-32e5-44e5-b38b-9a479d1a94bd"
      },
    })
  }, [page])
  function onChange(current: number) {
    setPage(page => page = current)
  }

  return (
    //整个页面
    <div className={style.regard}>
      <div className={style.regards}>
        <h2>留言板</h2>
        <p>[请勿灌水 🤖]</p>
      </div>
    </div>
  )
}

