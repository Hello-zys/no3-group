import style from './AsRegards.less';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'umi';
import Image from '@/component/Image'
export default function IndexPage() {

  const [page, setPage] = useState(1)

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: "regards/getregardsList",
      payload: {
        page,
        id: "a5e81ffe-0ad0-4be9-acca-c0462b1b98a1"
      },
    })
  }, [page])

  return (
    <div>
      <div className={style.container}>
        {/* 图片 */}
        <div className={style.imgs}>
          <Image>
          <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" alt="" />
          </Image>
        </div>
        {/* 文字 */}
        <div className={style.texts}>
          <h2>这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。</h2>
        </div>
      </div>
    </div>
  );
}
