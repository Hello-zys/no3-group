import { NavLink, RouteComponentProps } from 'react-router-dom';
import Style from './index.less';
import { MonitorOutlined, SmileFilled, MehFilled } from '@ant-design/icons';
import { useEffect, useState ,useRef, memo} from 'react';
import React from 'react';
import Dialogs from '@/component/Dialog';
import cls from "classnames";
import { setLocale, useIntl, useSelector } from 'umi';
import { Input, Space } from 'antd';
import { AudioOutlined } from '@ant-design/icons';
import { getSearch } from '@/services';
import { useDebounceCallback } from '@react-hook/debounce';

// 建立mome
const Dialog=memo(Dialogs);

interface ISearchItem {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

const Layouts: React.FC<RouteComponentProps> = (props) => {
  const loading = useSelector(
    (state: { loading: { global: boolean } }) => state.loading.global,
  );
  const { Search } = Input;
  const [visible, setVisible] = useState(false);
  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: '#1890ff',
      }}
    />
  );
  const [searchList, setsearchList] = useState([]);

  const onSearch = async (value: string) => {
    // 请求数据
    let { data } = await getSearch(value);
    setsearchList(data);
  };
  const [ind, setInd] = useState(sessionStorage.getItem('defaultOne') || '0');
  const intl = useIntl();
  const list = [
    {
      path: '/',
      content: 'menu.article',
    },
    {
      path: '/archive',
      content: 'menu.archive',
    },
    {
      path: '/knowledge',
      content: 'menu.knowledge',
    },
    {
      path: '/page/regards',
      content: 'menu.message',
    },
    {
      path: '/page/asRegards',
      content: 'menu.about',
    },
  ];
  const [goTop, setgoTop] = useState(false); //置顶
  const [upNav, setupNav] = useState(false); //展开对话框
  const [searchShow, setsearchShow] = useState(false); //弹出搜索
  const [color, setColor] = useState(true); //改变颜色
  const dom = React.createRef<HTMLDivElement>();
  const [chan, setchan] = useState('menu.language.chinese');
  // 点击切换文字
  const changeChan = () => {
    chan === 'menu.language.english'
      ? setLocale('zh-CN', false)
      : setLocale('en-US', false);
    chan === 'menu.language.english'
      ? setchan('menu.language.chinese')
      : setchan('menu.language.english');
  };

    // 记录滚动高度
    let page=0;
  // 滚动事件  利用react的一个防抖钩子
  const scroll = useDebounceCallback ((e: any) => {
      // 置顶
      if (e.target.scrollTop > 50) {
        setgoTop(true);
      } else {
        setgoTop(false);
      }
        // 滑动屏幕
        if (page >= e.target.scrollTop) {
          dom.current && (dom.current!.style.top = '0');
        } else {
          // 向下划
          dom.current && (dom.current!.style.top = '-100%');
        }
        page=e.target.scrollTop;
  },100,true);//第三个boolean是为了看是否在事件进行中才执行

  // 切换颜色
  const changeColor = () => {
    setColor(!color);
    if (!color) {
      document.body.classList.remove('dark');
    } else {
      document.body.classList.add('dark');
    }
  };
  return (
    <div className={Style.rootbox} onScroll={scroll} id="scrollableDiv">
      <Dialog bol={searchShow}>
        <h2 style={{ color: '#fff', padding: '0 20px' }}>
          文章搜索
          <span
            style={{
              float: 'right',
              color: '#fff',
              fontSize: 22,
              cursor: 'pointer',
            }}
            onClick={() => setsearchShow(false)}
          >
            X
          </span>
        </h2>
        <div style={{ width: '80%', margin: '0 auto' }}>
          <Space direction="vertical" style={{ width: '100%', height: '2rem' }}>
            <Search
              placeholder="请输入你想要更改的内容"
              onSearch={onSearch}
              style={{ width: '100%', margin: '0 auto' }}
            />
          </Space>
        </div>
        <ul
          style={{
            color: '#fff',
            lineHeight: '30px',
            cursor: 'pointer',
            width: '80%',
            margin: '0 auto',
          }}
        >
          {searchList.length > 0 &&
            searchList.map((item: ISearchItem) => {
              return (
                <li
                  key={item.id}
                  onClick={() => {
                    setsearchShow(false);
                    props.history.push('/archive/' + item.id);
                  }}
                >
                  {item.title}
                </li>
              );
            })}
        </ul>
      </Dialog>
      {goTop ? (
        <span
          className={Style.gotop}
          onClick={() => {
            document.querySelector('header+main')!.scrollIntoView({
              behavior: 'smooth',
              block: 'start',
            });
          }}
        >
          ↑
        </span>
      ) : null}
      <header className={Style.header}>
        <div className={Style.box}>
          <div className={Style.wapper} ref={dom}>
            <div className={Style.content}>
              <div className={Style.icon}>
                <a href="/">
                  <img
                    src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
                    alt=""
                  />
                </a>
              </div>
              <div className={Style.main}>
                <div
                  className={cls(Style.mobileNav, visible ? Style.active : '')}
                  onClick={() => {setVisible(!visible);setupNav(!upNav)}}
                >
                  <span className={Style.stick}></span>
                  <span className={Style.stick}></span>
                  <span className={Style.stick}></span>
                </div>
              </div>
              <nav className={upNav ? Style.checkednav : ''}>
                <ul className={Style.ul}>
                  {list.map((item, index) => {
                    return (
                      <li
                        key={index}
                        onClick={() => {
                          setInd(index + '');
                          sessionStorage.setItem('defaultOne', index + '');
                          setupNav(false);
                        }}
                      >
                        <NavLink
                          to={item.path}
                          style={{ color: ind === `${index}` ? 'red' : '' }}
                        >
                          {intl.formatMessage({ id: item.content })}
                        </NavLink>
                      </li>
                    );
                  })}
                  <li
                    className={Style.right}
                    onClick={() => {
                      setsearchShow(true);
                    }}
                  >
                    <MonitorOutlined />
                  </li>
                  <li className={Style.right}>
                    <span onClick={changeColor}>
                      {color ? <SmileFilled /> : <MehFilled />}
                    </span>
                  </li>
                  <li className={Style.right}>
                    <span onClick={changeChan}>
                      {intl.formatMessage({ id: chan, defaultMessage: '' })}
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>

      <main>{props.children}</main>

      <div className={Style.footerText}>
        <div>
          <span>picture</span>
          <span>picture</span>
        </div>
        <div>
          <p>
            Designed by{' '}
            <a href="https://github.com/fantasticit/wipi" target="_blank">
             cnpm Fantasticit
            </a>{' '}
            .{' '}
            <a href="https://admin.blog.wipi.tech/" target="_blank">
              后台管理
            </a>
          </p>
          <p>Copyright © 2021. All Rights Reserved.</p>
          <p>皖ICP备18005737号</p>
        </div>
      </div>
    </div>
  );
};

export default Layouts;
