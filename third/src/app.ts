import { RequestConfig } from 'umi';
import { createLogger } from 'redux-logger';
import { message } from 'antd';
import Nprogress from 'nprogress';
import 'nprogress/nprogress.css';
Nprogress.configure({ showSpinner: false });

//禁掉线上的console
process.env.NODE_ENV==='production'?console.log=()=>{}:null
//dva的日志配置
export const dva =process.env.NODE_ENV==='production'?{}: {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};

// 全局路由切换配置
export function onRouteChange({ matchedRoutes }: any) {
  Nprogress.start();
  setTimeout(() => {
    Nprogress.done();
  }, 2000);
}

// 网络请求配置
let showError = false;
const baseUrl = '//api.blog.wipi.tech';
export const request: RequestConfig = {
  timeout: 10000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [(url, options) => {
    return {
      url: `${baseUrl}${url}`,
      options,
    };
  }],
  responseInterceptors: [response => {
    const codeMaps: { [key: number]: string } = {
      400: '错误的请求',
      403: '禁止访问',
      404: '找不到资源',
      500: '服务器内部错误',
      502: '网关错误。',
      503: '服务不可用，服务器暂时过载或维护。',
      504: '网关超时。',
    };
    if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
      if (!showError) {
        showError = true;
        message.error({
          content: codeMaps[response.status],
          onClose: () => showError = false
        });
      }

    }
    return response;
  }],
};