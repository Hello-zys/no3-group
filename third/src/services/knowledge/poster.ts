import { IPostItem } from "@/types"
import { request } from "umi"

// 生成海报
export function getPoster(data: IPostItem) {
  return request(`/api/poster`, {
    method: "POST",
    data
  })
}