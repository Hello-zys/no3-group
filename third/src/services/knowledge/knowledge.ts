import { request } from "umi"

// 获取knowledge列表
export function getKnowledgeList(page: number, pageSize = 12, status = "publish") {
  return request("/api/knowledge", {
    params: {
      page,
      pageSize,
      status
    }
  })
}

//文章分类
export function getCategoryList(articleStatus = "publish") {
  return request("/api/category", {
    params: {
      articleStatus
    }
  })
}

//获取knowledge列表 ---详情页
export function getKnowledgeDetail(id: string) {
  return request(`/api/knowledge/${id}`, {
    params: {
      id
    }
  })
}

// 获取开始阅读详情页
export function getStartReading(id: string) {
  return request(`/api/knowledge/${id}/views`, {
    method: "POST"
  })
}


// 点赞
export function getLike(id: string, type: string, name: string) {
  return request(`/api/${name}/${id}/likes`, {
    method: "POST",
    data: {
      type: type
    }
  })
}