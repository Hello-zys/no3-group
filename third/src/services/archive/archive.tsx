import { request } from "umi"
// 获取文章列表

export function getArchiveList() {
  return request('/api/article/archives')
}
// 获取文章列表
export function getCategory(articleStatus = "publish") {
  return request("/api/category", {
    params: {
      articleStatus
    }
  })
}
// 获取详情数据
export function getDeatil(id: string) {
  return request(`/api/article/${id}/views`, {
    method: "POST"
  })
}
// 获取详情推荐阅读数据
export function getDetailRecommend(articleId:string){
   return request("/api/article/recommend",{
      params:{
         articleId
      }
   })
}
