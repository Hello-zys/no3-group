import { request } from 'umi';

// 获取文章列表
export function getregardsList({ page, pageSize = 6, id }: { page: number, pageSize?: number, id: string }) {
  return request(`/api/comment/host/${id}`, {
    params: {
      page, pageSize
    }
  })
}
