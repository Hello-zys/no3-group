import { ArchiveModelState } from "@/models/archive";
import { ArticleModelState } from "@/models/article";
import { KnowledgeModelState } from "@/models/knowledge"
import { regardsModelState } from "@/models/regards"

export interface IRootState {
  article: ArticleModelState
  loading: {
    effects: {
      "archive/getArchiveList": boolean,
      "article/getRecommend": boolean,
      "archive/getCategory": boolean,
      "article/getArticlelabel": boolean,
      "article/getUpdataFelist": boolean,
      "article/getFeListName": boolean,
      "article/getFeList": boolean
      'knowledge/getKnowledgeList': boolean;
      'knowledge/getCategoryList': boolean;
      'regards/getregardsList': boolean;
      'knowledge/getKnowledgeDetail': boolean;
  }
}
}
export interface IknowState {
  knowledge: KnowledgeModelState,
  loading: {
    effects: {
      'knowledge/getKnowledgeList': boolean;
      'knowledge/getCategoryList': boolean;
      'article/getRecommend': boolean;
      'regards/getregardsList': boolean;
      'knowledge/getKnowledgeDetail': boolean;
      'archive/getArchiveList': boolean;
      'archive/getCategory': boolean;
    }
  },
}
export interface IRegardsState {
  regards: regardsModelState
}

export interface IArchiveState {
  archive: ArchiveModelState,
 
}
