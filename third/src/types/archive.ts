export interface IArchiveStateItem {
  archive: any;
  '2020': _2020;
  '2021': _2021;
  loading:{
    effects:{
     "archive/getArchiveList": boolean
      "archive/getCategory": boolean
      "archive/getDeatil": boolean
      "archive/getDetailRecommend": boolean
      "article/getRecommend": boolean
    }
  }

}
export interface Gostate {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
interface _2021 {
  July: July2[];
  May: June[];
  April: June[];
  March: June[];
  February: June[];
}

export interface July2 {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

interface _2020 {
  July: July[];
  June: June[];
  May: May[];
  April: June[];
  March: June[];
  February: June[];
}

interface May {
  id: string;
  title: string;
  cover: string;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

interface June {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

interface July {
  id: string;
  title: string;
  cover: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
export interface ArchiveDetailItem {
  id: string;
  ip: string;
  userAgent: string;
  url: string;
  count: number;
  address: string;
  browser: string;
  engine: string;
  os: string;
  device: string;
  createAt: string;
  updateAt: string;
}
export interface DeatailRecommendItem {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: Category;
  tags: Category[];
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

