import { getKnowledgeList, getCategoryList, getKnowledgeDetail, getStartReading, getLike } from "@/services"
import { IknowState } from "@/types"
import { IknowledgeItem, ICategoryItem, IknowledgeDetail, IStartReading } from "@/types/knowledge"
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

// 数据项类型
interface IArrItem {
  level: string;
  id: string;
  text: string;
}
export interface KnowledgeModelState {
  knowledge: IknowledgeItem[];
  category: ICategoryItem[];
  detailItem: Partial<IknowledgeDetail>;
  startReading: Partial<IStartReading>;
  toc: IArrItem[]
}
export interface IndexModelType {
  namespace: 'knowledge';
  state: KnowledgeModelState;
  effects: {
    getKnowledgeList: Effect;
    getCategoryList: Effect,
    getKnowledgeDetail: Effect;
    getStartReading: Effect;
    getLike: Effect
  };
  reducers: {
    save: Reducer<KnowledgeModelState>;
  };
}

const IndexModel: IndexModelType = {
  namespace: 'knowledge',

  state: {
    knowledge: [],
    category: [],
    detailItem: {},
    startReading: {},
    toc: []
  },
  effects: {
    *getKnowledgeList({ payload }, { call, put }) {
      let result = yield call(getKnowledgeList)
      // console.log('result ++++0000+++++...',result.data[0])
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { knowledge: result.data[0] }
        })
      }
    },

    *getCategoryList({ payload }, { call, put }) {
      let result = yield call(getCategoryList)
      // console.log('result ++++0000+++++...',result)
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { category: result.data }
        })
      }
    },

    *getKnowledgeDetail({ payload }, { call, put }) {
      let result = yield call(getKnowledgeDetail, payload)  //payload===>id
      // console.log('result ++++0000+++888888++...',result)
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { detailItem: result.data }
        })
      }
    },

    *getStartReading({ payload }, { call, put }) {
      let result = yield call(getStartReading, payload)
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { startReading: result.data, toc: eval(result.data.toc) }
        })
      }
    },
    // 喜欢
    *getLike({ payload }, { call, put }) {
      let result = yield call(getLike, payload.id, payload.type, payload.name)
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { startReading: result.data }
        })
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;