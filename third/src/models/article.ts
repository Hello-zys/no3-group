import { getArticleList, getRecommend, getFeList, getArticlelabel, getFeListName } from '@/services';
import { IRootState, FeListItem, IArticlelableItem } from '@/types';
import { IArticleItem } from '@/types/article';
import { Effect, FormattedPlural, ImmerReducer, Reducer, Subscription } from 'umi';

export interface ArticleModelState {
  recommend: IArticleItem[];
  articleList: IArticleItem[];
  articleCount: number;
  feList: FeListItem[],
  feListCount: number,
  articlelableList: IArticlelableItem[],
  feListName: string
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    getFeList: Effect;
    getArticlelabel: Effect;
    getUpdataFelist: Effect;
    getFeListName: Effect
  };
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const IndexModel: ArticleModelType = {
  namespace: 'article',

  state: {
    recommend: [],
    articleList: [],
    articleCount: 0,
    feList: [],
    feListCount: 0,
    articlelableList: [],
    feListName: ""
  },

  effects: {
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        })
      }
    },

    *getArticleList({ payload }, { call, put, select }) {
      let articleList = yield select((state: IRootState) => state.article.articleList);
      let result = yield call(getArticleList, payload);
      if (result.statusCode === 200) {
        articleList = payload === 1 ? result.data[0] : [...articleList, ...result.data[0]]
        yield put({
          type: 'save',
          payload: {
            articleList,
            articleCount: result.data[1]
          }
        })
      }
    },
    *getArticlelabel({ payload }, { call, put }) {
      let result = yield call(getArticlelabel)
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            articlelableList: result.data
          }
        })
      }
    },

    *getFeList({ payload }, { call, put, select }) {
      let feList = yield select((state: IRootState) => state.article.feList);
      let result = yield call(getFeList, payload.page, payload.id);
      if (result.statusCode === 200) {
        feList = payload === 1 ? result.data[0] : [...feList, ...result.data[0]]
        yield put({
          type: 'save',
          payload: {
            feList,
            feListCount: result.data[1]
          }
        })
      }
    },
    *getUpdataFelist({ payload }, { call, put, select }) {
      let result = yield call(getFeList, payload.page, payload.id);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            feList: result.data[0],
            feListCount: result.data[1]
          }
        })
      }
    },
    *getFeListName({ payload }, { call, put }) {
      let result = yield call(getFeListName, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            feListName: result.data.label,
          }
        })
      }
    }
  },


  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
};

export default IndexModel;