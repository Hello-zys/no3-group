import React from 'react'
import { Input } from 'antd';
import { SmileOutlined, CloseOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { useState } from 'react';
import { Form, Checkbox } from 'antd';
import emojis from '../../utils/emojis'
import style from './index.less'

interface Props {
  bol: boolean,
  callback: () => void
}

export default function Popous({ bol = false, callback }: Props) {

  let [kg, setkg] = useState(false);
  let [kg1,setkg1]=useState(false);
  let [srk,setsrk]=useState('')
  const { TextArea } = Input;
const onChange = (e: any) => {
  
  console.log(e, '1111');
  console.log(e.target.value);
  setsrk(e.target.value)
 
  
};
const onFinish = (values: any) => {
  console.log('Success:', values);
  const { name, email } = values
  const zc = /[1-9]\d{7,10}\@qq\.com/
  if (name && zc.test(email)) {
    localStorage.setItem('user', JSON.stringify(values))
    setkg(false)
  } else {
    alert("邮箱不正确")
  }
};

const onFinishFailed = (errorInfo: any) => {
  console.log('Failed:', errorInfo);
};

const onBQ=(item: string)=>{
  console.log(item,'item');
  setsrk(srk=>srk+=item)
  console.log(srk,'sj');
  
}

  return (
    <div id="popouts">
      <div className={style.aa}>
        {/* 文本框 */}
        <div className={style.textboxs}>
          <div className={style.input} onClick={() => { 
            if(localStorage.getItem('user')){
              setkg(false)
            }else{
              setkg(true)
            }
           }}>
            <TextArea placeholder="请输入评论内容（支持Markdown）" allowClear onChange={onChange} value={srk} className={style.inpu} />
          </div>
          <footer style={{ paddingBottom: bol ? "0" : "10px" }}>
            <div className={style.icon} onClick={() => { 
               if(localStorage.getItem('user')){
                setkg(false)
                setkg1(!kg1)
              }else{
                setkg(true)
              }
             }} >
              <span className={style.bq}>  <SmileOutlined /></span>
              <span>表情</span>
            
            </div>
            
                {
                kg1&&<div className={style.aq}>
                   {
                    Object.values(emojis).map((item,index)=>{
                       return <li key={index}  onClick={()=>{onBQ(item)}} >
                        {item}
                       </li>
                    })
                   }
                </div>
              }
          
            {/* 发布按钮 */}
            <div> {bol && <Button className={style.ss} onClick={callback}>收回</Button>}  <Button className={style.ss}  >发布</Button></div>
          </footer>
        </div>
      </div>
      {/* 弹框 */}
      {
        kg ? <div className={style.Popouts}>
          <div className={style.BouncedContent}>
            <div className={style.taps}>
              <div className={style.lef}>请设置你的信息</div>
              <div className={style.rig} onClick={() => { setkg(false) }} ><CloseOutlined /></div>
            </div>
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <Form.Item
                label="名称"
                name="name"
                rules={[{ required: true, message: 'Please input your username!' }]}
                // style={{width:500}}
                className={style.ws}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="邮箱"
                name="email"
                rules={[{ required: true, message: 'Please input your password!' }]}
                className={style.ws}
              >
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ offset: 8, span: 16 }} className={style.wz}>
                <div className={style.yy}>
                  <Button htmlType="submit" className={style.ssk1} onClick={() => { setkg(false) }}>
                    取消
                  </Button>
                  <Button htmlType="submit" className={style.ssk}>
                    设置
                  </Button>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
          : null
      }
    </div>
  )
}


