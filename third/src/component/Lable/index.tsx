import styles from './index.less'; // 启用css-module
import React from "react"
import { IArticlelableItem } from "@/types";
import { NavLink } from 'react-router-dom';
import Spin from "../Spin"

interface Props {
  bol: boolean,
  articlelableList: IArticlelableItem[]
}
const Lable: React.FC<Props> = (props) => {
  return <div className={styles.label}>
    <span className={styles.labelTitle}>文章标签</span>
    <div className={styles.label_content}>
      {props.bol && <Spin />}
      {
        props.articlelableList?.map(item => {
          return <p key={item.id} className={styles.label_item}>
            <NavLink to={`/git/${item.value}`}>{item.label}</NavLink>[{item.articleCount}]
                </p>
        })
      }
    </div>
  </div>

}

export default Lable