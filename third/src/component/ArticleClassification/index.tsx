import { Gostate } from "@/types";
import styles from "./index.less"
import React from "react";
import Spin from "@/component/Spin";

// 类型验证
interface Props {
    CategoryList: Gostate[],
    callback:(id:string)=>void,
    bol:boolean
}
const SortItem: React.FC<Props> = (props) => {
    return (
        <div className={styles.right_bot}>
            <div className={styles.right_botHeader}>
                <span>文章分类</span>
            </div>
            {props.bol && <Spin/>}
            <div className={styles.rightBot_Botcon}>
                <ul className={styles.right_botContent}>
                    {
                        props.CategoryList.map(item => {
                            return <li style={{ opacity: 1, height: "37px", transform: "none" }} key={item.id} onClick={()=>props.callback(item.value)}>
                                <p>
                                    <span>{item.label}</span>
                                    <span>共计{item.articleCount}篇文章</span>
                                </p>
                            </li>
                        })
                    }
                </ul>
            </div>

        </div>
    )
}
export default SortItem