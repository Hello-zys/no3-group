import { IArticleItem } from '@/types/article';
import moment from "@/utils/moment";
import React, { } from "react"
import { NavLink } from 'umi';
import styles from "./index.less"
import Spin from "@/component/Spin"

interface Props {
  recommend: IArticleItem[],
  bol: boolean
}
const RecommendedReading: React.FC<Props> = (props) => {
  return <div className={styles.right} >
    <div className={styles.recommend}>
      <span className={styles.recommendTitle}>推荐阅读</span>
      {props.bol && <Spin />}
      {
        props.recommend?.map(item => {
          return <NavLink to={`/archive/${item.id}`} className={styles.recommend_text} key={item.id}>
            <span>{item.title}</span>
            <span>.</span>
            <span className={styles.time}>
              {moment(item.createAt, "YYYYMMDD").fromNow()}
            </span>
          </NavLink>
        })}
    </div>
  </div>
}

export default RecommendedReading