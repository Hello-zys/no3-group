
## 2021.8.11
- [闭包](word文档)
- [react为什么使用react](word文档)
## 2021.8.12
- [react组件通信](word文档)
- [react-thunk](word文档)
## 2021.8.13
- [umi](https://umijs.org/zh-CN/docs/getting-started)
- [TypeScript类型 泛型](https://juejin.cn/post/6995842578914476062)
## 2021.8.15
- [js常用的dom操作](word文档)
- [js原生事件绑定](word文档)
## 2021.8.16
- [UmiJS](https://umijs.org/zh-CN/docs/getting-started)
- [阿里开源可插拔 React 跨端框架- Umi](https://zhuanlan.zhihu.com/p/68679617)

## 2021.8.17
- [vue-cli工程](word文档)
- [vue的核心](word文档)

## 2021.8.18
- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)
- [vue](https://jasonandjay.github.io/study/zh/vue/)