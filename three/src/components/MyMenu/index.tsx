import { Layout, Popover, Button, Menu, } from 'antd'
import { Link } from 'umi';
import styles from './index.less'
const { Header, Sider } = Layout;
import { data } from '@/assets/data'
import React from 'react';
const { SubMenu } = Menu;
const content = [
  <div>
    <p><Link to='/article/amEditor'>新建文章-协同编辑器</Link></p>
    <p><Link to='/article/editor'>新建文章</Link></p>
    <p><Link to='/page/editor'>新建页面</Link></p>
  </div>
]
interface Iheader {
  collapsed: boolean
}
const MyMenu: React.FC<Iheader> = (props) => {

  return <Sider collapsed={props.collapsed}>
    <div className="logo">
      <div>
        <div className={styles.logo}>
          <img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" style={{ width: "32px", height: "32px", opacity: 1 }} />
          <span>管理后台</span>
        </div>
        <div className={styles.logo}>
          <Popover content={content}>
            <Button type="primary"><b>+</b>&nbsp;新建</Button>
          </Popover>
        </div>
      </div>
    </div>
    <Menu theme='dark' mode="inline" defaultSelectedKeys={['1']}>
      {
        data.map((item, index) => {
          return !item.children ? <Menu.Item key={index} icon={<item.icon />}>
            <Link to={item.path}><span>{item.name}</span></Link>
          </Menu.Item> : <SubMenu key='sub1' title={item.name} icon={<item.icon />}>
            {
              item.children.map((item1) => {
                return <Menu.Item key={item1.path} icon={<item1.icon />}>
                  <Link to={item1.path}>
                    <span>{item1.name}</span>
                  </Link>
                </Menu.Item>
              })
            }
          </SubMenu>
        })
      }
    </Menu>
  </Sider>
}

export default MyMenu;