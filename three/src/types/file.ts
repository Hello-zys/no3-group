export interface IValuess {
    originalname: string;
    type:string;
}
export interface IPayload {
    pageSize?:number;
    page:number
    originalname?: string;
    type?:string;
}
export interface IFileItem {
    id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
}
