export interface RootObject {
    id: string;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    totalAmount?: any;
    isPay: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    tags: any[];
    category?: any;
    label: string;
    value: string;
    articleCount: number;
  }

  