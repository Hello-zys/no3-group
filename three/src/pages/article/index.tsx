import React, { Component } from 'react'
import useStore from '@/context/useStore';
<<<<<<< HEAD
import { RootObject } from '@/types/article';
import { Form, Button, Input, Table, Cascader } from 'antd';
=======
import { RootObject } from '@/type/article';
import { Form, Button, Input, Table, Cascader ,Badge} from 'antd';
>>>>>>> cef213e5be6cad2c15545e8b9dec69d4e064db63
import { observer } from 'mobx-react-lite'
import { Key, useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import Style from './index.less'
<<<<<<< HEAD

const Article: React.FC = () => {
=======
const Article = () => {
>>>>>>> cef213e5be6cad2c15545e8b9dec69d4e064db63
    const [form] = Form.useForm();
    const store = useStore();
    console.log(store);
    const [page, setPage] = useState(1);
    const [params, setParams] = useState({})
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])

    //获取文章管理列表
    useEffect(() => {
        store.article.getArticleList(page, params)
    }, [page, params]);

    //条件查询
    function submit() {
        let values = form.getFieldsValue();
        let params: { [key: string]: string } = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
        setPage(1)
    }
    //表格选中操作
    function onSelectChange(selectedRowKeys: Key[], selectedRows: RootObject[]) {
        setSelectedRowKeys(selectedRowKeys)
    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }
    const options = [
        {
            value: '已发布',
            label: '已发布',
        },
        {
            value: '草稿',
            label: '草稿',
        }
    ];
    const option = [
        {
            value: '前端a撒大苏打',
            label: '前端a撒大苏打',
        },
        {
            value: '后端',
            label: '后端',
        },
        {
            value: '烦烦烦方法',
            label: '烦烦烦方法',
        },
        {
            value: 'AAA',
            label: 'AAA',
        },
        {
            value: 'SSS',
            label: 'SSS',
        },
        {
            value: '123',
            label: '123',
        },
        {
            value: '阿松大',
            label: '阿松大',
        },
    ];
    function onChange(value: any) {
        console.log(value);
    }

    // Just show the latest item.
    function displayRender(label: string | any[]) {
        return label[label.length - 1];
    }
    const columns = [
        {
            title: '标题',
            dataIndex: 'title'
        }, {
            title: '状态',
            render:(item:RootObject)=>{
                return item.status? <Badge status="success" text="已发布"/>: <Badge status="warning" text="草稿" />
            }
        }, {
            title: '分类',
            dataIndex: 'category'
        }, {
            title: '标签',
<<<<<<< HEAD
            dataIndex: 'tags'
=======
            dataIndex: 'tags',
            render: (tags: []) => {
                if (tags.length > 0) {
                    return <span></span>
                } else {

                }
            }
>>>>>>> cef213e5be6cad2c15545e8b9dec69d4e064db63
        }, {
            title: '阅读量',
            render: (item: RootObject) => {
                return <span>
                    <sup data-show="true" className="ant-scroll-number ant-badge-count" title="0" >
                        {item.views}
                    </sup>
                </span>
            }

        }, {
            title: '喜欢数',
            // dataIndex: 'likes'
            render: (item: RootObject) => {
                return <span>
                    <sup data-show="true" className="ant-scroll-number ant-badge-count" title="0" >
                        {item.likes}
                    </sup>
                </span>
            }
        }, {
            title: '发送时间',
            dataIndex: 'createAt'
        }, {
            title: '操作',
            render: (item: RootObject) => {
                return <div>
                    <Button>编辑</Button>
                    <Button>首焦推荐</Button>
                    <Button>查看访问</Button>
<<<<<<< HEAD
                    <Button>删除</Button>
=======
                    <Button onClick={() => store.article.deleteArticle([item.id])}>删除</Button>
>>>>>>> cef213e5be6cad2c15545e8b9dec69d4e064db63
                </div>
            }
        }
    ]
    return (
        <div className={Style.tou}>
            <div className={Style.top}>
                <Form>
                    <span className={Style.title}>标题：<Input placeholder="请输入文章标题" /></span>
                    <span className={Style.topzt}>
                        状态：<Cascader
                            options={options}
                            expandTrigger="hover"
                            displayRender={displayRender}
                            onChange={onChange}
                        />
                    </span>
                    <span className={Style.topfl}>
                        分类：<Cascader
                            options={option}
                            expandTrigger="hover"
                            displayRender={displayRender}
                            onChange={onChange}
                        />
                        <div className={Style.btn}>
                            <Button htmlType="submit" type="primary">搜索</Button>
                            <Button htmlType="reset">重置</Button>
                        </div>
                    </span>
                </Form>
            </div>
            <div className={Style.button}>
                <p className={Style.xj}><Button htmlType="submit" type="primary">新建</Button></p>
                <Table
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={store.article.articleList}
                    rowKey="id"
                    pagination={{ showSizeChanger: true }}
                />
            </div>
        </div>

    )
}

export default observer( Article)

   