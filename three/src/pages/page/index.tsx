import React from 'react';
import 'antd/dist/antd.css';
import { Button, Popover } from 'antd';
function Index() {
  const content = (
    <div>
      <p>Content</p>
      <p>Content</p>
    </div>
  );
  return <div>
    页面管理
    <Popover content={content} title="Title">
      <Button type="primary">Hover me</Button>
    </Popover>
  </div>
}
export default Index;