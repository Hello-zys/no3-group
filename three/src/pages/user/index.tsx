import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Select, Table, Tag, Space } from 'antd';
import useStore from '@/context/useStore';
import styles from './index.less';
import './index.less';
interface Props {}

const userindex = (props: Props) => {
  const state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
  };
  const store = useStore();
  let [page, pageState] = useState(1);
  let [pageSize, pageSizeState] = useState(12);
  useEffect(() => {
    store.UserItem.getuserItem(page, pageSize);
  }, []);
  const columns = [
     {
       title:'23',
     },
    {
      title: '账户',
      dataIndex: 'name',
      key: 'name',
      render: (text: any) => <a>{text}</a>,
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: '角色',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: '注册日期',
      dataIndex: 'data',
      key: 'data',
    },
    {
      title: '状态',
      key: 'tags',
      dataIndex: 'tags',
      render: (tags: any) => (
        <>
          {tags.map((tag: any) => {
            let color = tag.length > 5 ? 'geekblue' : 'green';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: '操作',
      key: 'action',
     
      render: (_text: any) => (
        <Space size="middle">
          <a>禁用</a>
          <a>授权</a>
        </Space>
      ),
    },
  ];
  const data = store.UserItem.userItemList.map((item, index) => {
    return {
      key: `${index}`,
      name: `${item.name}`,
      age: 32,
      address: '访客',
      data: `${item.updateAt}`,
      tags: ['可用', '不可用'],
    };
  });
  const tal = store.UserItem.userItemList.length;
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: this.onSelectChange,
  // };
  // const start = () => {
  //   this.setState({ loading: true });
  //   // ajax request after empty completing
  //   setTimeout(() => {
  //     this.setState({
  //       selectedRowKeys: [],
  //       loading: false,
  //     });
  //   }, 1000);
  // };
  const Delall=()=>{
    console.log(12);
    
  }
  return (
    <div className={styles.user}>
      <div className={styles.top}>
        <Form
          className={styles.inputs}
          // name="basic"
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-around',
            paddingTop: '2%',
          }}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="账户"
            // name="username"
            rules={[{ required: true }]}
          >
            {/* <span> 账户&nbsp;:</span>{' '} */}
            <Input
              style={{ width: '10rem', display: 'flex' }}
              type="text"
              placeholder="请输入用户名账户好"
            />
          </Form.Item>

          <Form.Item
            label="邮箱"
            // name="username"
            rules={[{ required: true }]}
          >
            {/* <span> 邮箱&nbsp;:</span>{' '} */}
            <Input style={{ width: '10rem' }} placeholder="请输入账户邮箱" />
          </Form.Item>

          <Form.Item label="角色">
            <Select style={{ width: '10rem' }}>
              <Select.Option value="demo">管理员</Select.Option>
              <Select.Option value="demo">访客</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item label="状态">
            {/* <span> 状态&nbsp;:&nbsp;</span> */}
            <Select style={{ width: '10rem' }}>
              <Select.Option value="demo">可用</Select.Option>
              <Select.Option value="demo">不可用</Select.Option>
            </Select>
          </Form.Item>
        </Form>
        <Form className={styles.new}>
          <Form.Item
            wrapperCol={{ offset: 8, span: 16 }}
            style={{ marginRight: '1rem' }}
          >
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
          <Form.Item
            wrapperCol={{ offset: 8, span: 16 }}
            className={styles.reset}
            style={{ marginRight: '2rem' }}
          >
            <Button type="primary" htmlType="submit" onClick={Delall} >
              重置
            </Button>
          </Form.Item>
        </Form>
      </div>
      <Table
        columns={columns}
        className={styles.bale}
        dataSource={data}
        pagination={{
          total: pageSize,
          // showTotal'总共数据',
          defaultPageSize: 5,
          defaultCurrent: 1,
        }}
        style={{
          background: '#fff',
          padding: '2% 2%',
          margin: '2% 2%',
          overflowX: 'auto',
        }}
      />

      {/* {store.UserItem.userItemList.map((item, index) => {
        return <div key={index}>{item.name}</div>;
      })} */}
    </div>
  );
};

export default userindex;
