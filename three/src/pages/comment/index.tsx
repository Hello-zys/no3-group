import useStore from '@/context/useStore';
import { ICommentItem } from '@/types';
import { Form, Input, Select, Button, Table, Badge,Popover } from 'antd';
import { Key, useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import './index.less';
import 'antd/dist/antd.css';
import React from 'react';

interface IForm{
  [key:string]:string|boolean
}
const { useForm } = Form;
const Comment: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  const [page, setPage] = useState(1);
  const [params,setParams]=useState<IForm>({})
  const [selectedRowKeys,setSelectedRowKeys]=useState<Key[]>([])

  useEffect(() => {
    store.comment.getComment(page,params);
  }, [page,params]);
  function submit() {
    let values = form.getFieldsValue();
    let params:IForm={}
    for(let key in values){
      values[key]&&(params[key]=values[key])
    }
    setParams(params)
  }
  const columns = [
    {
      title: '状态',
      render: (row: ICommentItem) => {
        return row.pass?<Badge status='success' text='通过'/>:<Badge status='warning' text='未通过'/>
      },
    },
    {
      title: '称呼',
      dataIndex: 'name',
    },
    {
      title: '联系方式',
      dataIndex: 'email',
    },
    {
      title: '原始内容',
      render:(item:ICommentItem)=>{
        return <Popover content={item.content} title='评论详情-原始内容'>
          <Button type='primary'>查看内容</Button>
        </Popover>
      }
    },
    {
      title: 'HTML内容',
      render:(item:ICommentItem)=>{
        return <Popover content={<div dangerouslySetInnerHTML={{__html:item.html}}></div>} title='评论详情-HTML内容'>
          <Button type='primary'>查看内容</Button>
        </Popover>
      }
    },
    {
      title: '管理文章',
      render:(item:ICommentItem)=>{
        return <Popover content={<iframe src={'https://creation.shbwyz.com'+item.url}></iframe>} title='页面预览'>
          <Button type='primary'>查看内容</Button>
        </Popover>
      }
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
    },
    {
      title: '父级评论',
      dataIndex: 'parentCommentId',
    },
    {
      title: '操作',
      render: (row: ICommentItem) => {
        return (
          <p>
            <Button onClick={()=>store.comment.updateComment([row.id],{pass:true})}>通过</Button>
            <Button onClick={()=>store.comment.updateComment([row.id],{pass:false})}>拒绝</Button>
            <Button>回复</Button>
            <Button onClick={()=>store.comment.deleteComment([row.id])}>删除</Button>
          </p>
        );
      },
    },
  ];

  function onSelectChange(selectedRowKeys:Key[],items:ICommentItem[]){
    setSelectedRowKeys(selectedRowKeys)
  }
  const rowSelection={
    selectedRowKeys,
    onChange:onSelectChange
  }

  return (
    <div>
      <div className="head">
        <div className="box">
          称呼：
          <input type="text" placeholder=" 请输入称呼" />
        </div>
        <div className="box">
          Email：
          <input type="text" placeholder=" 请输入邮箱" />
        </div>
        <div className="box">
          状态：
          <select name="" id="">
            <option value="true">已通过</option>
            <option value="false">未通过</option>
          </select>
        </div>
        <div className="btn">
          <Button type="primary">搜索</Button>
          <Button type="dashed">重置</Button>
        </div>
      </div>

      {selectedRowKeys.length?<section>
        <Button onClick={()=>store.comment.updateComment(selectedRowKeys as string[],{pass:true})}>通过</Button>
        <Button onClick={()=>store.comment.updateComment(selectedRowKeys as string[],{pass:false})}>拒绝</Button>
        <Button onClick={()=>store.comment.deleteComment(selectedRowKeys as string[])}>删除</Button>
      </section>:null}

      <div className='cont'>
      <Table
        columns={columns}
        dataSource={store.comment.commentList}
        rowSelection={rowSelection}
        rowKey='id'
      />
      </div>
    </div>
  );
};

export default observer(Comment);
