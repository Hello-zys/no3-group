import useStore from '@/context/useStore';
import { IMailItem } from '@/types';
import { Form, Button, Input, Table, Breadcrumb, Layout } from 'antd'
import { Link } from 'umi';
import { observer } from 'mobx-react-lite'
import { Key, useEffect, useState } from 'react';
import './index.css';
const { Header, Content } = Layout;
const Main = () => {
    const [form] = Form.useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [params, setParams] = useState({});
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])

    // 获取邮件列表
    useEffect(() => {
        store.mail.getMailList(page, params);
    }, [page, params]);

    // 条件查询
    function submit() {
        let values = form.getFieldsValue();
        let params: { [key: string]: string } = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
        setPage(1);
    }
    // 表格选中操作
    function onSelectChange(selectedRowKeys: Key[], selectedRows: IMailItem[]) {
        setSelectedRowKeys(selectedRowKeys);
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }

    const columns = [{
        title: '发件人',
        dataIndex: 'from'
    }, {
        title: '收件人',
        dataIndex: 'to'
    }, {
        title: '主题',
        dataIndex: 'subject'
    }, {
        title: '发送时间',
        dataIndex: 'createAt'
    }, {
        title: '操作',
        render: (item: IMailItem) => {
            return <Button onClick={() => store.mail.deleteMail([item.id])}>删除</Button>
        }
    }]
    return <div className='mail'>
        <Header className="header">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link to="/home"></Link>
                    工作台</Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to='/mail'>邮件管理</Link>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Header>
        <Content>
            <div className="con_mail">
                <div>
                    <Form style={{ padding: '24px 12px', backgroundColor: '#fff', display: 'flex', flexDirection: 'column' }}
                        form={form}
                        onFinish={submit}
                    >
                        <p>
                            <Form.Item
                                style={{ rowGap: 0 }}
                                label="发件人"
                                name="from"
                            >
                                <Input type="text" placeholder="请输入发件人" />
                            </Form.Item>
                            <Form.Item
                                style={{ rowGap: 0 }}
                                label="收件人"
                                name="to"
                            >
                                <Input type="text" placeholder="请输入收件人" />
                            </Form.Item>
                            <Form.Item
                                style={{ rowGap: 0 }}
                                label="主题"
                                name="subject"
                            >
                                <Input type="text" placeholder="请输入主题" />
                            </Form.Item>
                        </p>
                        <p>
                            <div style={{ width: '100%', textAlign: 'right', marginRight: '16px', position: 'relative' }}>
                                <Button style={{ width: '63.84px', height: '32px' }} htmlType="submit" type="primary">搜索</Button>
                                <Button style={{ width: '63.84px', height: '32px', marginLeft: '8px' }} htmlType="reset">重置</Button>
                            </div>
                        </p>
                    </Form>
                    <p>
                        {selectedRowKeys.length ? <Button onClick={() => store.mail.deleteMail(selectedRowKeys as string[])}>删除</Button> : null}
                        <Button onClick={() => {
                            setPage(1);
                            setParams({ ...params });
                        }}>刷新</Button>
                    </p>
                    <Table style={{ backgroundColor: '#fff' }}
                        rowSelection={rowSelection}
                        // loading={Boolean(store.mail.mailList.length)}
                        columns={columns}
                        dataSource={store.mail.mailList}
                        rowKey="id"
                        pagination={{ showSizeChanger: true }}
                    />
                </div>
            </div>
        </Content>
    </div>
}

export default observer(Main);
