import { deleteMail, getMailList } from '@/services';
import { IMailItem } from '@/types';
import {makeAutoObservable, runInAction} from 'mobx';
import {message} from 'antd';

class Mail{
    mailListCount = 1;
    mailList: IMailItem[] = [];
    constructor(){
        makeAutoObservable(this);
    }

    // 获取邮件列表
    async getMailList(page = 1, params={}){
        let result = await getMailList(page, params);
        if (result.data){
            runInAction(() =>{
                this.mailList = result.data[0];
                this.mailListCount = result.data[1];
            })
        }
    }

    // 删除邮件
    async deleteMail(ids: string[]){
        message.loading('操作中');
        Promise.all(ids.map(id=>deleteMail(id)))
        .then(res=>{
            message.destroy();
            message.success('批量删除成功');
            this.getMailList();
        })
        .catch(err=>{
            message.success('批量删除成功');
            this.getMailList();
        })
    }
}

export default Mail;