<<<<<<< HEAD
import {getArticleList,deleteArticle} from '@/services/article'
import {RootObject } from '@/types'
=======
import {getArticleList,deleteArticle,getCategoryList} from '@/services/article'
import {RootObject } from '@/type'
>>>>>>> cef213e5be6cad2c15545e8b9dec69d4e064db63
import {makeAutoObservable,runInAction} from 'mobx'
import {message} from 'antd';
class Article{
    articleListCount=1;
    articleList:RootObject[]=[];
    categoryListCount=1;
    categoryList:RootObject[]=[];
    constructor(){
        makeAutoObservable(this)
    }
    //获取文章列表
    async getArticleList(page=1,params={}){
        let result=await getArticleList(page,params);
        console.log(result);
        
        if(result.data){
            runInAction(()=>{
                this.articleList=result.data[0];
                this.articleListCount=result.data[1];
            })
        }
    }
    // 删除文章
    async deleteArticle(ids: string[]){
        message.loading('操作中');
        Promise.all(ids.map(id=>deleteArticle(id)))
        .then(res=>{
            message.destroy();
            message.success('批量删除成功');
            this.getArticleList();
        })
        .catch(err=>{
            message.success('批量删除成功');
            this.getArticleList();
        })
    }
    //获取文章分类列表
    async getCategoryList(){
        let result=await getCategoryList();
        console.log(result);
        
        if(result.data){
            runInAction(()=>{
                this.categoryList=result.data
            })
        }
    }
}
export default Article