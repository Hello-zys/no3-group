import {getuserItem} from '@/services'
import {IUseritem} from '@/types'
import { makeAutoObservable,runInAction } from 'mobx';
// import {  } from '_mobx@6.3.3@mobx/dist/internal';
class UserItem {
    userItem = 1;
    userItemList:IUseritem[]=[]
    constructor(){
         makeAutoObservable(this)
    }
    async getuserItem(page:number,pageSize:number){
        let result = await getuserItem(page,pageSize)
        console.log(result);

        if(result.data){
            runInAction(()=>{
                this.userItem = result.data[1],
                this.userItemList = result.data[0]
            })
        }
    }
}
export default UserItem