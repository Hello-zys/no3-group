// 引入状态管理模块
import User from './user'
import Mail from './mail'
import Article from './article'
import UserItem from './useritem'
import Comment from './comment'
import File from './file'
export default {
    user: new User,
    mail: new Mail,
    article:new Article,
    UserItem: new UserItem(),
    comment:new Comment(),
    file:new File,
}