import {request} from 'umi'
import {IPayload} from '@/types'
export const  getFileLists=({page,pageSize=12,originalname,type}:IPayload)=>{
    return request('/api/file',{params:{page:page,pageSize:pageSize,originalname,type}})
}
export const setDeletes=(id:string)=>{
    return request(`/api/file/${id}`,{method:'DELETE'})
}
