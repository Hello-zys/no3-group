import { IMailQuery } from '@/types';
import { request } from 'umi';

// 获取邮件接口
export function getMailList(page=1, params:Partial<IMailQuery>={}, pageSize=12){
    return request(`/api/smtp?page=${page}&pageSize=${pageSize}`, {params})
}

// 删除邮件
export function deleteMail(id: string){
    return request(`/api/smtp/${id}`, {
        method: 'DELETE'
    })
}