import {IUseritem} from '@/types/useritem'
import {request} from 'umi'
export function getuserItem(page:number,pageSize:number){
    return request(`/api/user?page=${page}&pageSize=${pageSize}`)

}