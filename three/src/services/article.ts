import {request} from 'umi';
import {RootObject} from '@/types'
//获取article列表
export function getArticleList(page=1,params:Partial<RootObject>={},pageSize=12){
    return request(`/api/article?page=${page}&pageSize=${pageSize}`,{params})
}
// 删除文章
export function deleteArticle(id: string){
    return request(`/api/article/${id}`, {
        method: 'DELETE'
    })
}
//文章分类管理列表
export function getCategoryList(){
    return request(`/api/category`)
}