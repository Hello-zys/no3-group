import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 开启antd
  antd: {
    dark: true,
    compact: true,
  },
  title: '工作台',
  links: [
    {
      rel: 'icon',
      href: '	https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png'
    }
  ],
});
