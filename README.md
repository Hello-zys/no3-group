
# 王宝玉

## 2021.9.16
1.文章阅读
- [JavaScript中Let和Const的使用方法](https://juejin.cn/post/7001770069961228295)

2.源码阅读
- 面试题

3.leecode刷题
4.项目进度
- [x] 小程序首页的功能
## 2020.9.15
 1.文章阅读
 - [React Hooks 响应式布局](https://juejin.cn/post/6844904089164185607)

 2.源码阅读
 - [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
 3.leecode刷题
 - [乘积最大子数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmk3rv/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
## 2020.9.14
 1.文章阅读
 - [JS表达式中的token匹配规则](https://zhuanlan.zhihu.com/p/27766326)
 2.源码阅读
 - [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
 4.项目进度
 - [x] 分项目
 - [x] 页面排版
## 2020.9.13
 1.文章阅读
 - [深入浅出 React Hooks](https://juejin.cn/post/6844903858662014983)

 2.源码阅读
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

3.leecode刷题
- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)
 4.项目进度
 - [x] 路由配置
 - [x] 分模块
 - [x] 文章页面排版
 - [x] 文章页面删除
 - [x] 文章分类
 - [x] 文章标签管理
 - [x] 文章页面跳编辑页面
## 2020.9.12
1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [前端面试知识点（二）](https://juejin.cn/post/6996815121855021087)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.事件处理](https://zh-hans.reactjs.org/docs/handling-events.html)

 3.leecode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

 4.项目进度
 - [x] 路由配置
 - [x] 分模块
 - [x] 文章页面排版
 - [x] 文章页面删除
 - [x] 文章分类
 - [x] 文章标签管理
 - [x] 文章页面跳编辑页面

 ## 2020.9.10
1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [React 灵魂 23 问 8-16](https://zhuanlan.zhihu.com/p/304213203)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.useMemo知识](https://zhuanlan.zhihu.com/p/348796468)

 3.leecode刷题
 - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

 4.项目进度
 - [x] 路由配置
 - [x] 分模块
 - [x] 文章页面排版
 - [x] 文章页面删除
 - [x] 文章分类
 - [x] 文章标签管理
## 2021.9.9
1.文章阅读
- [UmiJS](https://umijs.org/zh-CN/docs/getting-started)
- [阿里开源可插拔 React 跨端框架- Umi](https://zhuanlan.zhihu.com/p/68679617)

2.源码阅读
- [.random](https://www.lodashjs.com/docs/lodash.random)
- [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
  - [x] 文章页面删除
  - [x] 文章分类
## 2021.9.5
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
- [打乱数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn6gq1/)
4. 项目进度
- [x]完善项目

## 2021.9.3
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API 服务--前两节](https://juejin.cn/book/6844733825164148744)
2. leetcode 刷题
- 面试题
3. 源码阅读
* ### [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
4. 项目进度
- 完善项目

## 2021.9.2
1. 文章阅读

- [从路由到 vue-router 源码，带你吃透前端路由](https://juejin.cn/post/6942520773156438023)

2. 源码阅读

- [call-apply实现](https://github.com/jasonandjay/js-code/blob/master/original/call-apply.js)

3. leecode 刷题

- [翻转字符串里的单词](https://leecode-cn.com/leetbook/read/array-and-string/crmp5/)

4. 项目进度

 ### 2021.9.1
1. 文章阅读
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3. LeeCode刷题
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
4. 项目进度

- 关于页面渲染
- 删除搜索功能

## 2021.8.31
1. 文章阅读
- 看看面试题
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 页面排版

## 2021.8.30
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. leetcode 刷题
3. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
4. 项目进度
## 2021.8.29
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
-
## 2021.8.27
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
3. 源码阅读
- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/views/useState.js)
4. 项目进度
- [x] `小楼又春风`
---
## 2021.08.26
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
- [深入分析Promise](https://juejin.cn/post/6945319439772434469)
3. LeeCode刷题
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x]排版
- [x]跳详情
- [x]国际化
- [x]请求加载
- [x]图片预览
## 2021.8.25
1. 文章阅读
### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
- 暂无
3. leecode 刷题
### [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
### [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
## 2021.08.24
1. 文章阅读
- [如何写一本掘金小册](https://juejin.cn/book/6844723704639782920)
2. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
4. 项目进度
-
## 2021.08.23
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读

3. LeeCode刷题
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x] 排版
- [x] 跳详情
- [x] 场景切换
- [x] 排版

## 2021.08.22
1. 文章阅读
- [ES5、Es6数组方法](https://juejin.cn/post/6945996368880091173)
2. 源码阅读
- [深入分析Promise](https://juejin.cn/post/6945319439772434469)
3. LeeCode刷题
4.项目进度
- [x] 详情页
- [x] 场景切换
- [x] 排版
## 2021.08.20
1. 文章阅读
- [如何写一本掘金小册](https://juejin.cn/book/6844723704639782920)
2. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
4. 项目进度
- [x] 详情页
- [x] 场景切换
- [x] 排版
## 2021.08.19
1. 文章阅读
- [JS原型与原型链](https://www.jianshu.com/p/dee9f8b14771)
2. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
4. 项目进度
- [x] 详情页
- [x] 场景切换
- [x] 排版
## 2021.08.18
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
3. LeeCode刷题
- [整数反转](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- [x]排版
- [x]跳详情
- [x]国际化
- [x]请求加载
- [x]图片预览
## 2021.08.17
1. 文章阅读
- [Es6 对象解构的用法与用途](https://juejin.cn/post/6941597799121158157)
2. 源码阅读
- [React.children.map](https://blog.csdn.net/hjc256/article/details/99943772)
- [React-使用循环并实现删除和修改](https://blog.csdn.net/Candy_mi/article/details/90726577?utm_term=mapreact%E5%88%A0%E9%99%A4&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-2-90726577&spm=3001.4430)
3. LeeCode 刷题
4. 项目进度
- [x]排版
- [x]跳详情
=======
## 2021.08.16
1. 文章阅读
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [只出现一次的数字](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- 排版
- 跳详情
## 2021.08.15
1. 文章阅读
- [通用篇 - 代码块的使用](https://juejin.cn/book/6844733783166418958/section/6844733783208361992)
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [删数相加](https://leetcode-cn.com/problems/add-two-numbers/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
- [字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
暂无
## 2021.08.13
1. 文章阅读
- [React-Router使用总结](https://juejin.cn/post/6844903566553923591)
- [JavaScript 中哪一种循环最快呢？](https://juejin.cn/post/6930973929452339213)
2. 源码阅读
- [Swiper](https://github.com/nolimits4web/swiper/blob/master/src/react/swiper.js)
3. LeeCode刷题
- [位数1的个数](https://leecode-cn.com/problems/number-of-1-bits/)
4. 项目进度
## 2021.8.12
1. 文章阅读
- [这样入门js抽象语法树(AST)](https://juejin.cn/post/6942016231214055454)
2. 源码阅读
- [Vue源码为什么v-for的优先级比v-if的高？](https://juejin.cn/post/6941995130144587789)
3. LeeCode刷题
- [只出现一次的数字](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
- 暂无
---
## 2021.8.11
1. 文章阅读
- [怎么理解VUE，VUE的数据驱动原理是什么，解释MVVM框架](https://blog.csdn.net/AN0692/article/details/79209004)
- [Es6 对象解构的用法与用途](https://juejin.cn/post/6941597799121158157)
2. 源码阅读
- [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
3. LeeCode刷题
- 暂无
4. 项目进度
- [x] git仓库创建
- [x] git环境
- [x] git命令
---
# 鲁燕艳
  ## 2021.9.2
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [webpack源码研究](https://juejin.cn/post/6844903903675285511)

  3.leecode刷题
  - [分割回文串](https://leetcode-cn.com/problems/palindrome-partitioning/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 评论页面
  ## 2021.9.1
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读

  3.leecode刷题
  - [实现strStr()](https://leetcode-cn.com/problems/string-to-integer-atoi/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 评论页面
  ## 2021.8.31
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读

  3.leecode刷题
  - [字符串转换整数](https://leetcode-cn.com/problems/string-to-integer-atoi/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 评论页面排版
  ## 2021.8.30
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [移除元素](https://leetcode-cn.com/problems/remove-element/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  ## 2021.8.29
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [react-hooks](https://juejin.cn/post/6944863057000529933)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [组合总和](https://leetcode-cn.com/problems/combination-sum/)

  4.项目进度
  - [x] 路由配置
  ## 2021.8.27
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [Jenkins](https://juejin.cn/post/6887751398499287054)

  2.源码阅读


  3.leecode刷题
  - [最大数](https://leetcode-cn.com/problems/largest-number/)

  4.项目进度
 - [x] 页面排版
 - [x] 鼠标滑过效果
 - [x] 点击跳转详情
 - [x] 详情页面排版
 - [x] 点赞分享
  ## 2021.8.26
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

  2.源码阅读
  - [router](https://juejin.cn/post/6844903818203758600)

  3.leecode刷题
  - [三数之和](https://leetcode-cn.com/problems/3sum/)

  4.项目进度
 - [x] 页面排版
 - [x] 鼠标滑过效果
 - [x] 点击跳转详情
 - [x] 详情页面排版
 - [x] 点赞分享
  ## 2021.8.25
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

  2.源码阅读
  - [.hook](https://zh-hans.reactjs.org/docs/hooks-intro.html)

  3.leecode刷题
  - [删除有序数组重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)

  4.项目进度
 - [x] 页面排版
 - [x] 鼠标滑过效果
 - [x] 点击跳转详情
 - [x] 详情页面排版
 - [x] 点赞分享
  ## 2021.8.24
  1.文章阅读
  - [hook](https://juejin.cn/post/6844903954539626510)
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [.hook](https://zh-hans.reactjs.org/docs/hooks-intro.html)

  3.leecode刷题
  - [回文数](https://leetcode-cn.com/problems/palindrome-number/)

  4.项目进度
 - [x] 页面排版
 - [x] 鼠标滑过效果
 - [x] 点击跳转详情
 - [x] 详情页面排版
 - [x] 点赞分享
  ## 2021.8.23
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

 2.源码阅读
 - [.vue Class与Style 绑定](https://cn.vuejs.org/v2/guide/class-and-style.html)
 - [.vue Prop](https://cn.vuejs.org/v2/guide/components-props.htmll)

 3.leecode刷题
 - [ 括号生成](https://leetcode-cn.com/problems/generate-parentheses/)
  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
  - [x] 文章页面删除
## 2021.9.1
 1.文章阅读
 - [理解 es6 class 中 constructor 方法 和 super 的作用](https://juejin.cn/post/6844903638674980872)
 - [var、let、const三者区别](https://juejin.cn/post/6925641096152399880)

 2.源码阅读
 - [.Effect Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

 3.leecode刷题
 - [回文数](https://leetcode-cn.com/problems/palindrome-number/)
  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
## 2021.8.31
1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [移除元素](https://leetcode-cn.com/problems/remove-element/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
## 2021.8.30
 1.文章阅读
 - [Vue生命周期](https://juejin.cn/post/6844903878538821640)
 - [React Hooks 最佳实践](https://juejin.cn/post/6844904165500518414)

 2.源码阅读
 - [redux源码解读](https://juejin.cn/post/6844903600456466445)
 3.leecode刷题
 - [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

 4.项目进度
 - [x] 运营后台路由配置
 - [x] 分模块
## 2021.8.29
 1.文章阅读
 - [从浅拷贝与深拷贝发现 JSON.stringify 的 “魅力”](https://juejin.cn/post/7001454317450297380)
 - [深入理解JavaScript作用域和作用域链](https://juejin.cn/post/6844903797135769614)

 2.源码阅读
 - [.vue 插槽](https://cn.vuejs.org/v2/guide/components-slots.html)
 - [.vue 动态组件 & 异步组件](https://cn.vuejs.org/v2/guide/components-dynamic-async.html)

 3.leecode刷题
 - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

 4.项目进度
 - [x] 运营后台路由配置
## 2021.8.27
 1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [前端面试知识点（二）](https://juejin.cn/post/6996815121855021087)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.事件处理](https://zh-hans.reactjs.org/docs/handling-events.html)

 3.leecode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
## 2021.8.26
 1.文章阅读
 - [Node.js基础语法和ES6新属性](https://juejin.cn/post/6844904004208558094)
 - [2021年我的前端面试准备](https://juejin.cn/post/6989422484722286600)

 2.源码阅读
 - [.vue 条件渲染](https://cn.vuejs.org/v2/guide/conditional.html)
 - [.Hook API 索引](https://zh-hans.reactjs.org/docs/hooks-reference.html)

 3.leecode刷题
 - [杨辉三角](https://leetcode-cn.com/problems/pascals-triangle/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
## 2021.8.25
 1.文章阅读
 - [理解 es6 class 中 constructor 方法 和 super 的作用](https://juejin.cn/post/6844903638674980872)
 - [var、let、const三者区别](https://juejin.cn/post/6925641096152399880)

 2.源码阅读
 - [.Effect Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

 3.leecode刷题
 - [回文数](https://leetcode-cn.com/problems/palindrome-number/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
 ## 2021.8.24
 1.文章阅读
 - [深入浅出 React Hooks](https://juejin.cn/post/6844903858662014983)
 - [react Hook之useMemo、useCallback及memo](https://juejin.cn/post/6844903954539626510)

 2.源码阅读
 - [.umi 插件](https://umijs.org/zh-CN/docs/plugin)
 - [.Hook 简介](https://zh-hans.reactjs.org/docs/hooks-intro.html)

 3.leecode刷题
 - [  二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
 ## 2021.8.23
 1.文章阅读
 - [理解 async/await](https://juejin.cn/post/6844903487805849613)
 - [promise经典面试题](https://juejin.cn/post/6844903632203153415)


# 李坤洋
 ## 2021.9.9
1.文章阅读
 <!-- - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854) -->
 - [React 灵魂 23 问 16-23](https://zhuanlan.zhihu.com/p/304213203)

 2.源码阅读
 <!-- - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html) -->
 - [.useMemo知识](https://zhuanlan.zhihu.com/p/348796468)

 3.leecode刷题
 - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

 4.项目进度
 - [x] 分项目
 - [x] 把页面打出来框架
 - [x] 从ant上借鉴
 - [x] 重置功能实现
 - [x] 全选反选功能


 ## 2021.9.8
1.文章阅读
 <!-- - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854) -->
 - [React 灵魂 23 问 8-16](https://zhuanlan.zhihu.com/p/304213203)

 2.源码阅读
 <!-- - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html) -->
 - [.useMemo知识](https://zhuanlan.zhihu.com/p/348796468)

 3.leecode刷题
 - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

 4.项目进度
 - [x] 分项目
 - [x] 把页面打出来框架
 - [x] 从ant上借鉴
 - [x] 重置功能实现
 - [x] 全选反选功能





## 2020.9.7
1.文章阅读
 <!-- - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854) -->
 - [React 灵魂 23 问 1-8](https://zhuanlan.zhihu.com/p/304213203)

 2.源码阅读
 <!-- - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html) -->
 - [.解密 setState](https://zhuanlan.zhihu.com/p/20328570)

 3.leecode刷题
 - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

 4.项目进度
 - [x] 分项目
 - [x] 把页面打出来框架
 - [x] 从ant上借鉴
 - [x] 重置功能实现
 - [x] 全选反选功能



## 2020.9.1
1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [前端面试知识点（二）](https://juejin.cn/post/6996815121855021087)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.事件处理](https://zh-hans.reactjs.org/docs/handling-events.html)

 3.leecode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮

## 2020.8.31
 1.文章阅读
 - [深入浅出 React Hooks](https://juejin.cn/post/6844903858662014983)

 2.源码阅读
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

3.leecode刷题
- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)
 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮

## 2020.8.30
 1.文章阅读
 - [JS表达式中的token匹配规则](https://zhuanlan.zhihu.com/p/27766326)
 2.源码阅读
 - [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮


## 2020.8.29
 1.文章阅读
 - [React Hooks 响应式布局](https://juejin.cn/post/6844904089164185607)

 2.源码阅读
 - [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
 3.leecode刷题
 - [乘积最大子数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmk3rv/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮
## 2020.9.1
1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [前端面试知识点（二）](https://juejin.cn/post/6996815121855021087)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.事件处理](https://zh-hans.reactjs.org/docs/handling-events.html)

 3.leecode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮

## 2020.8.31
 1.文章阅读
 - [深入浅出 React Hooks](https://juejin.cn/post/6844903858662014983)

 2.源码阅读
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

3.leecode刷题
- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)
 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮

## 2020.8.30
 1.文章阅读
 - [JS表达式中的token匹配规则](https://zhuanlan.zhihu.com/p/27766326)
 -

 2.源码阅读
 - [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮


## 2020.8.29
 1.文章阅读
 - [React Hooks 响应式布局](https://juejin.cn/post/6844904089164185607)
 -

 2.源码阅读
 - [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
 3.leecode刷题
 - [乘积最大子数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmk3rv/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮

## 2020.8.27
 1.文章阅读
 - [JS-闭包经典使用](https://zhuanlan.zhihu.com/p/379579795)
 -

 2.源码阅读
 - [web前端之闭包](https://blog.csdn.net/liao0801_123/article/details/82593335?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163010910416780357210891%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163010910416780357210891&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-82593335.first_rank_v2_pc_rank_v29&utm_term=%E5%89%8D%E7%AB%AF%E9%97%AD%E5%8C%85&spm=1018.2226.3001.4187)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [乘积最大子数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmk3rv/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮


## 2020.8.26
 1.文章阅读
 - [单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html)
 -

 2.源码阅读
 - [hash算法原理详解](https://blog.csdn.net/tanggao1314/article/details/51457585)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [分割回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xaxi62/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮

## 2020.8.25
 1.文章阅读
 - [浏览器的缓存机制](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)
 -

 2.源码阅读
 - [hash算法原理详解](https://blog.csdn.net/tanggao1314/article/details/51457585)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [分割回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xaxi62/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现
 - [x] 详情数据渲染 排版
 - [x] 点赞分享 点击高亮
## 2020.8.24
 1.文章阅读
 - [浏览器的缓存机制](https://zhuanlan.zhihu.com/p/53507705)
 -

 2.源码阅读
 - [.react-thunk](https://www.npmjs.com/package/react-thunk)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [除自身之外 数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现


 ## 2021.8.23
 1.文章阅读
- [webpack打包原理](https://segmentfault.com/a/1190000021494964?utm_source=tag-newest)

 2.源码阅读
- [React Hooks源码浅析](https://zhuanlan.zhihu.com/p/68842478)

 3.leecode刷题
 - [两数之和，求两数下标](https://leetcode-cn.com/problems/two-sum/submissions/)
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)


 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现
 - [x] 跳详情的实现

  ## 2021.8.22
 1.文章阅读
- [谷歌浏览器调试JavaScript小技巧！](https://blog.csdn.net/chris1299/article/details/118544775)
- [css元素的显示与隐藏](https://zhuanlan.zhihu.com/p/391574238)

 2.源码阅读
 - [.vue-router hooks](https://www.lodashjs.com/docs/lodash.difference)

 3.leecode刷题
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染
 - [x] 分页的实现

 ## 2021.8.20
 1.文章阅读
 - [var，let，const三者的特点和区别](https://juejin.cn/post/6991053348396859428)
 - [ES6 对象都新增了哪些属性](https://juejin.cn/post/6920062625346748429)

 2.源码阅读
 - [.vue Class与Style 绑定](https://cn.vuejs.org/v2/guide/class-and-style.html)
 - [.vue Prop](https://cn.vuejs.org/v2/guide/components-props.htmll)

 3.leecode刷题
 - [ 括号生成](https://leetcode-cn.com/problems/generate-parentheses/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染
 - [x] 页面视图数据的渲染

 ## 2021.8.19
 1.文章阅读
 - [React 路由](https://juejin.cn/post/6997578178566979591)
 - [什么是闭包](https://juejin.cn/post/6874829017006997511)

 2.源码阅读
- [.indexOf](https://www.lodashjs.com/docs/lodash.indexOf)
 3.leecode刷题
 - [ 三数之和](https://leetcode-cn.com/problems/3sum/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
 - [x] 时间的渲染

 ## 2021.8.18
 1.文章阅读
 - [Vue.js 3.2 关于响应式部分的优化](https://juejin.cn/post/6995732683435278344)
 - [React Hooks 响应式布局](https://juejin.cn/post/6844904089164185607)

 2.源码阅读
 - [.reactx](https://www.redux.org.cn/)
 - [.vue router](https://router.vuejs.org/zh/api/#router-link)

 3.leecode刷题
 - [ 所有可能的路径](https://leetcode-cn.com/problems/all-paths-from-source-to-target/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版

 ## 2021.8.17
 1.文章阅读
 - [30分钟精通React Hooks](https://juejin.cn/post/6844903709927800846)
 - [前端面试必考题：React Hooks 原理剖析](https://juejin.cn/post/6844904205371588615)

 2.源码阅读
 - [.react-thunk](https://www.npmjs.com/package/react-thunk)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [ 另一棵树的子树](https://leetcode-cn.com/problems/subtree-of-another-tree/)

 4.项目进度
 - [x] 分项目

 - [x] 跳详情的实现
 ## 2021.8.16
 1.文章阅读
 - [Umi@3.0+Ts+Antd@4.0从零搭建后台项目工程](https://juejin.cn/post/6868164601201033230)
 - [Ant Design Umi 项目创建](https://juejin.cn/post/6996281989229707301)

 2.源码阅读
 - [.react](https://zh-hans.reactjs.org/tutorial/tutorial.html)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [填充每个节点的下一个右侧节点指针 II](https://leetcode-cn.com/problems/populating-next-right-pointers-in-each-node-ii/)

 4.项目进度

 ## 2020.8.15
  1.文章阅读
 - [前端必知必会 | 学SVG看这篇就够了（一）](https://juejin.cn/post/6993211337509699620)
 - [前端必知必会 | 学SVG看这篇就够了（二）](https://juejin.cn/post/6993607549576544270)

 2.源码阅读
 - [.hook](https://zh-hans.reactjs.org/docs/hooks-intro.html)
 - [.vue](https://cn.vuejs.org/v2/guide/)

 3.leecode刷题
 - [两数之和，求两数下标](https://leetcode-cn.com/problems/two-sum/submissions/)

 4.项目进度

 ## 2020.8.13
 1.文章阅读
 - [闭包](word文档)
 - [css元素的显示与隐藏](https://zhuanlan.zhihu.com/p/391574238)

 2.源码阅读
 - [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
 - [.difference](https://www.lodashjs.com/docs/lodash.difference)

 3.leecode刷题
 - [除自身之外 数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

 4.项目进度
 - [x] 无


## 2021.8.12
1.文章阅读
 - [Vue响应式原理+Virtual DOM](https://juejin.cn/post/6995470002153324580)
 - [vue中修饰符.sync与v-model的区别](https://juejin.cn/post/6995474182494486559)

 2.源码阅读


3.leecode刷题
 - [在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/)

4.项目进度

## 2021.8.11

1.文章阅读

1.文章阅读
 - [vue面试总结](https://juejin.cn/post/6992370132148305927)
 - [React：组件](https://juejin.cn/post/6994707544484610078)

2.源码阅读


3.leecode刷题

4.项目进度


# 张亚美
## 2020.9.16
1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [react-hooks](https://juejin.cn/post/6944863057000529933)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [组合总和](https://leetcode-cn.com/problems/combination-sum/)

  4.项目进度
- [x] 分项目
- [x] 页面排版
- [x] 大牌优惠tab切换
## 2020.9.15
 1.文章阅读
 - [React Hooks 响应式布局](https://juejin.cn/post/6844904089164185607)

 2.源码阅读
 - [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
 3.leecode刷题
 - [乘积最大子数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmk3rv/)

 4.项目进度
 - [x] 分项目
 - [x] 页面排版
## 2020.9.14
 1.文章阅读
 - [JS表达式中的token匹配规则](https://zhuanlan.zhihu.com/p/27766326)
 2.源码阅读
 - [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
 4.项目进度
 - [x] 分项目
 - [x] 页面排版
## 2020.9.13
 1.文章阅读
 - [深入浅出 React Hooks](https://juejin.cn/post/6844903858662014983)

 2.源码阅读
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

3.leecode刷题
- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)
 4.项目进度
 - [x] 路由配置
 - [x] 分模块
 - [x] 文章页面排版
 - [x] 文章页面删除
 - [x] 文章分类
 - [x] 文章标签管理
 - [x] 文章页面跳编辑页面
## 2020.9.12
1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [前端面试知识点（二）](https://juejin.cn/post/6996815121855021087)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.事件处理](https://zh-hans.reactjs.org/docs/handling-events.html)

 3.leecode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

 4.项目进度
 - [x] 路由配置
 - [x] 分模块
 - [x] 文章页面排版
 - [x] 文章页面删除
 - [x] 文章分类
 - [x] 文章标签管理
 - [x] 文章页面跳编辑页面

 ## 2020.9.10
1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [React 灵魂 23 问 8-16](https://zhuanlan.zhihu.com/p/304213203)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.useMemo知识](https://zhuanlan.zhihu.com/p/348796468)

 3.leecode刷题
 - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

 4.项目进度
 - [x] 路由配置
 - [x] 分模块
 - [x] 文章页面排版
 - [x] 文章页面删除
 - [x] 文章分类
 - [x] 文章标签管理
## 2021.9.9
1.文章阅读
- [UmiJS](https://umijs.org/zh-CN/docs/getting-started)
- [阿里开源可插拔 React 跨端框架- Umi](https://zhuanlan.zhihu.com/p/68679617)

2.源码阅读
- [.random](https://www.lodashjs.com/docs/lodash.random)
- [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
  - [x] 文章页面删除
  - [x] 文章分类
## 2021.9.2
1.文章阅读
 - [var，let，const三者的特点和区别](https://juejin.cn/post/6991053348396859428)
 - [ES6 对象都新增了哪些属性](https://juejin.cn/post/6920062625346748429)

 2.源码阅读
 - [.vue Class与Style 绑定](https://cn.vuejs.org/v2/guide/class-and-style.html)
 - [.vue Prop](https://cn.vuejs.org/v2/guide/components-props.htmll)

 3.leecode刷题
 - [ 括号生成](https://leetcode-cn.com/problems/generate-parentheses/)
  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
  - [x] 文章页面删除
## 2021.9.1
 1.文章阅读
 - [理解 es6 class 中 constructor 方法 和 super 的作用](https://juejin.cn/post/6844903638674980872)
 - [var、let、const三者区别](https://juejin.cn/post/6925641096152399880)

 2.源码阅读
 - [.Effect Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

 3.leecode刷题
 - [回文数](https://leetcode-cn.com/problems/palindrome-number/)
  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
## 2021.8.31
1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [移除元素](https://leetcode-cn.com/problems/remove-element/)

  4.项目进度
  - [x] 路由配置
  - [x] 分模块
  - [x] 文章页面排版
## 2021.8.30
 1.文章阅读
 - [Vue生命周期](https://juejin.cn/post/6844903878538821640)
 - [React Hooks 最佳实践](https://juejin.cn/post/6844904165500518414)

 2.源码阅读
 - [redux源码解读](https://juejin.cn/post/6844903600456466445)
 3.leecode刷题
 - [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

 4.项目进度
 - [x] 运营后台路由配置
 - [x] 分模块
## 2021.8.29
 1.文章阅读
 - [从浅拷贝与深拷贝发现 JSON.stringify 的 “魅力”](https://juejin.cn/post/7001454317450297380)
 - [深入理解JavaScript作用域和作用域链](https://juejin.cn/post/6844903797135769614)

 2.源码阅读
 - [.vue 插槽](https://cn.vuejs.org/v2/guide/components-slots.html)
 - [.vue 动态组件 & 异步组件](https://cn.vuejs.org/v2/guide/components-dynamic-async.html)

 3.leecode刷题
 - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

 4.项目进度
 - [x] 运营后台路由配置
## 2021.8.27
 1.文章阅读
 - [webpack快速入门教程](https://juejin.cn/post/6996665311260835854)
 - [前端面试知识点（二）](https://juejin.cn/post/6996815121855021087)

 2.源码阅读
 - [.react State & 生命周期](https://zh-hans.reactjs.org/docs/state-and-lifecycle.html)
 - [.事件处理](https://zh-hans.reactjs.org/docs/handling-events.html)

 3.leecode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
## 2021.8.26
 1.文章阅读
 - [Node.js基础语法和ES6新属性](https://juejin.cn/post/6844904004208558094)
 - [2021年我的前端面试准备](https://juejin.cn/post/6989422484722286600)

 2.源码阅读
 - [.vue 条件渲染](https://cn.vuejs.org/v2/guide/conditional.html)
 - [.Hook API 索引](https://zh-hans.reactjs.org/docs/hooks-reference.html)

 3.leecode刷题
 - [杨辉三角](https://leetcode-cn.com/problems/pascals-triangle/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
## 2021.8.25
 1.文章阅读
 - [理解 es6 class 中 constructor 方法 和 super 的作用](https://juejin.cn/post/6844903638674980872)
 - [var、let、const三者区别](https://juejin.cn/post/6925641096152399880)

 2.源码阅读
 - [.Effect Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)
 - [.State Hook](https://zh-hans.reactjs.org/docs/hooks-overview.html)

 3.leecode刷题
 - [回文数](https://leetcode-cn.com/problems/palindrome-number/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
 ## 2021.8.24
 1.文章阅读
 - [深入浅出 React Hooks](https://juejin.cn/post/6844903858662014983)
 - [react Hook之useMemo、useCallback及memo](https://juejin.cn/post/6844903954539626510)

 2.源码阅读
 - [.umi 插件](https://umijs.org/zh-CN/docs/plugin)
 - [.Hook 简介](https://zh-hans.reactjs.org/docs/hooks-intro.html)

 3.leecode刷题
 - [  二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 - [x] 点击分享弹出分享内容
 ## 2021.8.23
 1.文章阅读
 - [理解 async/await](https://juejin.cn/post/6844903487805849613)
 - [promise经典面试题](https://juejin.cn/post/6844903632203153415)

 2.源码阅读
 - [.vue 深入响应式原理](https://cn.vuejs.org/v2/guide/reactivity.html)
 - [.react 使用 PropTypes 进行类型检查](https://zh-hans.reactjs.org/docs/typechecking-with-proptypes.html)

 3.leecode刷题
 - [ 两数之和](https://leetcode-cn.com/problems/two-sum/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 ## 2021.8.22
 1.文章阅读
 - [前端的深拷贝和浅拷贝](https://juejin.cn/post/6999096522973397022)
 - [ES6有什么新特性](https://juejin.cn/post/6844903944104181767)

 2.源码阅读
 - [.vue 插槽](https://cn.vuejs.org/v2/guide/components-slots.html)
 - [.vue Prop](https://cn.vuejs.org/v2/guide/components-props.htmll)

 3.leecode刷题
 - [ 组合总和](https://leetcode-cn.com/problems/combination-sum/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 - [x] 点击开始阅读跳入开始阅读页面右侧渲染
 ## 2021.8.20
 1.文章阅读
 - [var，let，const三者的特点和区别](https://juejin.cn/post/6991053348396859428)
 - [ES6 对象都新增了哪些属性](https://juejin.cn/post/6920062625346748429)

 2.源码阅读
 - [.vue Class与Style 绑定](https://cn.vuejs.org/v2/guide/class-and-style.html)
 - [.vue Prop](https://cn.vuejs.org/v2/guide/components-props.htmll)

 3.leecode刷题
 - [ 括号生成](https://leetcode-cn.com/problems/generate-parentheses/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 - [x] 点击开始阅读跳入开始阅读页面
 ## 2021.8.19
 1.文章阅读
 - [React 路由](https://juejin.cn/post/6997578178566979591)
 - [什么是闭包](https://juejin.cn/post/6874829017006997511)

 2.源码阅读
 - [.vue生命周期](https://cn.vuejs.org/v2/api/)
 - [.reactDom](https://zh-hans.reactjs.org/docs/react-dom.html)

 3.leecode刷题
 - [ 三数之和](https://leetcode-cn.com/problems/3sum/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色
 ## 2021.8.18
 1.文章阅读
 - [Vue.js 3.2 关于响应式部分的优化](https://juejin.cn/post/6995732683435278344)
 - [React Hooks 响应式布局](https://juejin.cn/post/6844904089164185607)

 2.源码阅读
 - [.reactx](https://www.redux.org.cn/)
 - [.vue router](https://router.vuejs.org/zh/api/#router-link)

 3.leecode刷题
 - [ 所有可能的路径](https://leetcode-cn.com/problems/all-paths-from-source-to-target/)

 4.项目进度
 - [x] 知识小册渲染数据
 - [x] 知识小册渲染右侧数据，点击改变颜色
 - [x] 点击知识小册左侧数据跳转详情页
 - [x] 点击知识小册左侧数据跳转详情页渲染数据，点击改变颜色

 ## 2021.8.17
 1.文章阅读
 - [30分钟精通React Hooks](https://juejin.cn/post/6844903709927800846)
 - [前端面试必考题：React Hooks 原理剖析](https://juejin.cn/post/6844904205371588615)

 2.源码阅读
 - [.react-thunk](https://www.npmjs.com/package/react-thunk)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [ 另一棵树的子树](https://leetcode-cn.com/problems/subtree-of-another-tree/)

 4.项目进度
 - [x] 点击知识小册左侧数据跳转详情页

 ## 2021.8.16
 1.文章阅读
 - [Umi@3.0+Ts+Antd@4.0从零搭建后台项目工程](https://juejin.cn/post/6868164601201033230)
 - [Ant Design Umi 项目创建](https://juejin.cn/post/6996281989229707301)

 2.源码阅读
 - [.react](https://zh-hans.reactjs.org/tutorial/tutorial.html)
 - [.vuex](https://vuex.vuejs.org/zh/guide/plugins.html#%E5%86%85%E7%BD%AE-logger-%E6%8F%92%E4%BB%B6)

 3.leecode刷题
 - [填充每个节点的下一个右侧节点指针 II](https://leetcode-cn.com/problems/populating-next-right-pointers-in-each-node-ii/)

 4.项目进度
 - [x] 知识小册渲染右侧数据，点击改变颜色

 ## 2020.8.15
  1.文章阅读
 - [前端必知必会 | 学SVG看这篇就够了（一）](https://juejin.cn/post/6993211337509699620)
 - [前端必知必会 | 学SVG看这篇就够了（二）](https://juejin.cn/post/6993607549576544270)

 2.源码阅读
 - [.hook](https://zh-hans.reactjs.org/docs/hooks-intro.html)
 - [.vue](https://cn.vuejs.org/v2/guide/)

 3.leecode刷题
 - [两数之和，求两数下标](https://leetcode-cn.com/problems/two-sum/submissions/)

 4.项目进度
 - [x] 知识小册渲染数据

 ## 2020.8.13
 1.文章阅读
 - [闭包](word文档)
 - [css元素的显示与隐藏](https://zhuanlan.zhihu.com/p/391574238)

 2.源码阅读
 - [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
 - [.difference](https://www.lodashjs.com/docs/lodash.difference)

 3.leecode刷题
 - [除自身之外 数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

 4.项目进度
 - [x] 无


## 2021.8.12
1.文章阅读
 - [Vue响应式原理+Virtual DOM](https://juejin.cn/post/6995470002153324580)
 - [vue中修饰符.sync与v-model的区别](https://juejin.cn/post/6995474182494486559)

 2.源码阅读


3.leecode刷题
 - [在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/)

4.项目进度

## 2021.8.11

1.文章阅读

1.文章阅读
 - [vue面试总结](https://juejin.cn/post/6992370132148305927)
 - [React：组件](https://juejin.cn/post/6994707544484610078)

2.源码阅读


3.leecode刷题

4.项目进度



# 赵江汇
## 2021.9.16
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [webpack源码研究](https://juejin.cn/post/6844903903675285511)

  3.leecode刷题
  - [分割回文串](https://leetcode-cn.com/problems/palindrome-partitioning/)

  4.项目进度
  - [x] tab切换
  ## 2021.9.15
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读

  3.leecode刷题
  - [实现strStr()](https://leetcode-cn.com/problems/string-to-integer-atoi/)

  4.项目进度
  ## 2021.9.14
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读

  3.leecode刷题
  - [字符串转换整数](https://leetcode-cn.com/problems/string-to-integer-atoi/)

  4.项目进度
  - [x] 路由配置
  ## 2021.9.13
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [移除元素](https://leetcode-cn.com/problems/remove-element/)

  4.项目进度
  ## 2021.9.10
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [react-hooks](https://juejin.cn/post/6944863057000529933)

  2.源码阅读
  - [redux源码解读](https://juejin.cn/post/6844903600456466445)

  3.leecode刷题
  - [组合总和](https://leetcode-cn.com/problems/combination-sum/)

  4.项目进度
  - [x] 路由配置
  ## 2021.9.9
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [Jenkins](https://juejin.cn/post/6887751398499287054)

  2.源码阅读


  3.leecode刷题
  - [最大数](https://leetcode-cn.com/problems/largest-number/)

  4.项目进度
 - [x] 页面排版
  ## 2021.9.8
  1.文章阅读
  - [前端面试题](https://jasonandjay.github.io/study/zh/standard/Start.html#conemu%E5%AE%89%E8%A3%85)
  - [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

  2.源码阅读
  - [router](https://juejin.cn/post/6844903818203758600)

  3.leecode刷题
  - [三数之和](https://leetcode-cn.com/problems/3sum/)

  4.项目进度
 - [x] 页面排版
## 2021.9.2
1.文章阅读
- [vue双向绑定](https://juejin.cn/post/7002554719897927694)

2.源码阅读
- [.castArray](https://www.lodashjs.com/docs/lodash.castArray)
- [.clone](https://www.lodashjs.com/docs/lodash.clone)

3.leecode刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

4.项目进度

- [x] 后台管理路由

## 2021.8.31
1.文章阅读
- [JavaScript错误处理](https://juejin.cn/post/7002595472929980447)

2.源码阅读
- [.castArray](https://www.lodashjs.com/docs/lodash.castArray)
- [.clone](https://www.lodashjs.com/docs/lodash.clone)

3.leecode刷题

-  [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

4.项目进度

- [x] 后台管理路由

## 2021.8.30
1.文章阅读
- [JavaScript中Let和Const的使用方法](https://juejin.cn/post/7001770069961228295)

2.源码阅读
- [.castArray](https://www.lodashjs.com/docs/lodash.castArray)
- [.clone](https://www.lodashjs.com/docs/lodash.clone)

3.leecode刷题

- [三数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvpj16/)
- [无重复字符的最长子串](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xv2kgi/)
- [最长回文子串](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvn3ke/)

4.项目进度

- [x] 后台管理路由
## 2021.8.29
1.文章阅读
- [JavaScript中Let和Const的使用方法](https://juejin.cn/post/7001770069961228295)

2.源码阅读
- [.castArray](https://www.lodashjs.com/docs/lodash.castArray)
- [.clone](https://www.lodashjs.com/docs/lodash.clone)

3.leecode刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4.项目进度

- [x] 后台管理路由

## 2021.8.27
1.文章阅读
- [Css盒子模型](https://juejin.cn/post/7001775912366637093)

2.源码阅读
- [.cloneDeep](https://www.lodashjs.com/docs/lodash.cloneDeep)
- [.cloneDeepWith](https://www.lodashjs.com/docs/lodash.cloneDeepWith)
- [.cloneWith](https://www.lodashjs.com/docs/lodash.cloneWith)

3.leecode刷题
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/)


4.项目进度

- [x] 任务分配

## 2021.8.26
1.文章阅读
- [React状态管理的一些思考（一）](https://juejin.cn/post/6995497136510992414)
- [TypeScript 数组的类型](https://juejin.cn/post/6995842578914476062)

2.源码阅读
- [.dorp](https://www.lodashjs.com/docs/lodash.drop)
- [.dorpRight](https://www.lodashjs.com/docs/lodash.dropRight)
- [.dropRightWhile](https://www.lodashjs.com/docs/lodash.dropRightWhile)

## 2020.8.25
 1.文章阅读
 - [JS-闭包经典使用](https://zhuanlan.zhihu.com/p/379579795)
 -

- [三数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvpj16/)
- [无重复字符的最长子串](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xv2kgi/)
- [最长回文子串](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvn3ke/)

4.项目进度

- [x] 搜索
- [x] 切换主题

## 2021.8.25
1.文章阅读

- [谷歌浏览器调试JavaScript小技巧！](https://zhuanlan.zhihu.com/p/181686230)
- [css元素的显示与隐藏](https://zhuanlan.zhihu.com/p/391574238)

2.源码阅读
- [.difference](https://www.lodashjs.com/docs/lodash.difference)
- [.differenceBy](https://www.lodashjs.com/docs/lodash.differenceBy)
- [.differenceWith](https://www.lodashjs.com/docs/lodash.differenceWith)

3.leecode刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

4.项目进度

 4.项目进度
 - [x] 无


## 2021.8.24
1.文章阅读
- [es6解构赋值 [a,b] = [b,a]的几个问题](https://juejin.cn/post/6994634734634696734)
- [记录常用的Nodejs文件API](https://juejin.cn/post/6995103734053208095)

2.源码阅读
- [.chunk](https://www.lodashjs.com/docs/lodash.chunk)
- [.compact](https://www.lodashjs.com/docs/lodash.compact)
- [.concat](https://www.lodashjs.com/docs/lodash.concat)

3.leecode刷题


- [除自身之外 数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

4.项目进度

- [x] XXXXXXXXXX

## 2021.08.23

1. 文章阅读

-[彻底弄懂css必考题（二）](https://juejin.cn/post/6999448979570163749)

2. 源码阅读

-[pullAllBy](https://www.lodashjs.com/docs/lodash.pullAllBy)

3. leecode 刷题

- [二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/submissions/)

4. 项目进度
- [x] - 跳详情页
- [x] - 渲染数据 并 点击切换
## 2021.08.22

1. 文章阅读

-[纯css实现beautiful按钮](https://juejin.cn/post/6999127555085172767)

2. 源码阅读

-[pull](https://www.lodashjs.com/docs/lodash.pull)


## 2021.8.20
1.文章阅读
- [js常用的dom操作](word文档)
- [js原生事件绑定](word文档)

2.源码阅读

- [componentDidMount](https://blog.csdn.net/weixin_34217773/article/details/88761178)

3.leecode刷题

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/

4.项目进度

- [√] 对于文章页面的基本排版
- [√] 通过接口渲染数据
- [√] 路由跳转

## 2021.8.19
1.文章阅读
- [react组件通信](word文档)
- [react-thunk](word文档)

2.源码阅读
- [react-hook](https://react.docschina.org/docs/getting-started.html)
- [UmiJS](https://umijs.org/zh-CN/docs/env-variables)

3.leecode刷题

- [回文数](https://leetcode-cn.com/submissions/detail/205872536/)

4.项目进度
- [√] 修改文章bug


## 2021.8.18
1.文章阅读
- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)
- [vue](https://jasonandjay.github.io/study/zh/vue/)

2.源码阅读

3.leecode刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

4.项目进度

- [√] 文章页面右侧小列表
- [√] 文章列表跳详情

## 2021.8.17
1.文章阅读
- [vue-cli工程](word文档)
- [vue的核心](word文档)

2.源码阅读
- [v-model的实现原理](https://www.jianshu.com/p/2012c26b6933)
- [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题


- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

4.项目进度

- [√] 首页的细化排版
- [√] 排插bug

## 2021.8.16
1.文章阅读
- [UmiJS](https://umijs.org/zh-CN/docs/getting-started)
- [阿里开源可插拔 React 跨端框架- Umi](https://zhuanlan.zhihu.com/p/68679617)

2.源码阅读
- [.random](https://www.lodashjs.com/docs/lodash.random)
- [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
- [字符串的排序](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)
- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4.项目进度

- [√] 首页的轮播图
- [√] 首页的细化排版

## 2021.8.15
1.文章阅读
- [js常用的dom操作](word文档)
- [js原生事件绑定](word文档)

2.源码阅读

- [componentDidMount](https://blog.csdn.net/weixin_34217773/article/details/88761178)

3.leecode刷题

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)


4.项目进度

- [√] 对于文章页面的基本排版
- [√] 通过接口渲染数据
- [√] 路由跳转

## 2021.8.13
1.文章阅读
- [umi](https://umijs.org/zh-CN/docs/getting-started)
- [TypeScript类型 泛型](https://juejin.cn/post/6995842578914476062)

2.源码阅读

- [umi](https://umijs.org/zh-CN/docs/getting-started)

3.leecode刷题

- [三数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvpj16/)
 [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)

4.项目进度

- [x] 项目

## 2021.8.12
1.文章阅读
- [react组件通信](word文档)
- [react-thunk](word文档)

2.源码阅读
- [react-hook](https://react.docschina.org/docs/getting-started.html)
- [UmiJS](https://umijs.org/zh-CN/docs/env-variables)

3.leecode刷题

- [回文数](https://leetcode-cn.com/submissions/detail/205872536/)

4.项目进度
- [x] xxxxxxxx


## 2021.8.11
1.文章阅读
- [闭包](word文档)
- [react为什么使用react](word文档)

2.源码阅读
- [react-hook](https://react.docschina.org/docs/getting-started.html)
- [UmiJS](https://umijs.org/zh-CN/docs/env-variables)

3.leecode刷题

- [回文数](https://leetcode-cn.com/submissions/detail/205872536/)

4.项目进度
- [x] xxxxx

# 李荣深


## 2021.9.1
1.文章阅读
- [2021年学vue还是react好](https://blog.csdn.net/weixin_48914851/article/details/113656770?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163054351516780265429606%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163054351516780265429606&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_click~default-8-113656770.first_rank_v2_pc_rank_v29&utm_term=react&spm=1018.2226.3001.4187)

2.源码阅读
- [.multiply](https://www.lodashjs.com/docs/lodash.multiply)
- [.round](https://www.lodashjs.com/docs/lodash.round)

3.leecode刷题

- [最大数](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xd3jrg/)
- [直线上最多的点数](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xdqyov/)

4.项目进度

- [x] 排版优化
- [x] 搜索记录排版
- [x] 路由配置




## 2021.8.31
1.文章阅读
- [React的基础知识](https://blog.csdn.net/chen4565/article/details/119220807?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163054351516780265429606%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163054351516780265429606&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-5-119220807.first_rank_v2_pc_rank_v29&utm_term=react&spm=1018.2226.3001.4187)

2.源码阅读
- [.transform](https://www.lodashjs.com/docs/lodash.transform)
- [.unset](https://www.lodashjs.com/docs/lodash.unset)

3.leecode刷题

- [完全平方数](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xd03l1/)
- [戳气球](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xdiww6/)

4.项目进度

- [x] 搜索记录排版
- [x] 路由配置





## 2021.8.30
1.文章阅读
- [react框架总结](https://blog.csdn.net/halations/article/details/84389261?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163054351516780265429606%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163054351516780265429606&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-3-84389261.first_rank_v2_pc_rank_v29&utm_term=react&spm=1018.2226.3001.4187)

2.源码阅读
- [.assign](https://www.lodashjs.com/docs/lodash.assign)
- [.assignIn](https://www.lodashjs.com/docs/lodash.assignIn)

3.leecode刷题

- [盛最多水的容器](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xw6oqi/)
- [单词接龙](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xwjmmm/)

4.项目进度

- [x] 搜索记录排版
- [x] 路由配置




## 2021.8.29
1.文章阅读
- [Web基础](https://blog.csdn.net/rzy1248873545/article/details/116000154?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163020265216780262573652%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163020265216780262573652&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-116000154.first_rank_v2_pc_rank_v29&utm_term=web&spm=1018.2226.3001.4187)

2.源码阅读
- [.VERSION](https://www.lodashjs.com/docs/lodash.VERSION)
- [.prototype.value](https://www.lodashjs.com/docs/lodash.prototype.value)

3.leecode刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/tencent/xxj50s/)
- [三数之和](https://leetcode-cn.com/leetbook/read/tencent/xxst6e/)

4.项目进度

- [x] 路由配置




## 2021.8.27
1.文章阅读
- [网络是怎样链接的](https://leetcode-cn.com/leetbook/read/how-networks-work/97clur/)

2.源码阅读
- [.templateSettings.imports._](https://www.lodashjs.com/docs/lodash.templateSettings.imports._)
- [.templateSettings.variable](https://www.lodashjs.com/docs/lodash.templateSettings.variable)

3.leecode刷题

- [颜色分类](https://leetcode-cn.com/leetbook/read/all-about-array/x9wv2h/)
- [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)

4.项目进度

- [x] 项目完善

## 2021.8.26
1.文章阅读
- [React的学习路径——从菜鸟到大牛](https://blog.csdn.net/yangxiaobo118/article/details/79823561?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162993753416780271597877%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162993753416780271597877&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-79823561.first_rank_v2_pc_rank_v29&utm_term=react&spm=1018.2226.3001.4187)

2.源码阅读
- [.stubFalse](https://www.lodashjs.com/docs/lodash.stubFalse)
- [.stubObject](https://www.lodashjs.com/docs/lodash.stubObject)

3.leecode刷题

- [斐波那契数列](https://leetcode-cn.com/leetbook/read/illustration-of-algorithm/50fxu1/)
- [替换空格](https://leetcode-cn.com/leetbook/read/illustration-of-algorithm/50ywkd/)

4.项目进度

- [x] 评论功能完善
- [x] 详情页面完善
- [x] 点击出现遮罩



## 2021.8.25
1.文章阅读
- [vue项目入门，包含vue全家桶的教程](https://blog.csdn.net/weixin_44989478/article/details/106425899?ops_request_misc=&request_id=&biz_id=102&utm_term=vue%E5%85%A8%E5%AE%B6%E6%A1%B6&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-6-106425899.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)

2.源码阅读
- [.defaultTo](https://www.lodashjs.com/docs/lodash.defaultTo)
- [.negate](https://www.lodashjs.com/docs/lodash.negate)

3.leecode刷题

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmy9jh/)
- [打乱数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmchc3/)

4.项目进度

- [x] 评论功能
- [x] 详情页面渲染



## 2021.8.24
1.文章阅读
- [vue项目搭建以及全家桶的使用详细教程](https://blog.csdn.net/weixin_34309543/article/details/88699807?ops_request_misc=&request_id=&biz_id=102&utm_term=vue%E5%85%A8%E5%AE%B6%E6%A1%B6&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-4-88699807.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)

2.源码阅读
- [.flow](https://www.lodashjs.com/docs/lodash.flow)
- [.flowRight](https://www.lodashjs.com/docs/lodash.flowRight)

3.leecode刷题

- [鸡蛋掉落](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmup75/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xapbdt/)

4.项目进度

- [x] 评论功能
- [x] 详情页面渲染


## 2021.8.23
1.文章阅读
- [以React为例，说说框架和性能](https://www.zhihu.com/market/paid_column/1167078439772721152/section/1169980035288162304?origin_label=search)
- [014，Ajax技术（非常重要）](https://zhuanlan.zhihu.com/p/402556852)

2.源码阅读
- [.times](https://www.lodashjs.com/docs/lodash.times)
- [.mixin](https://www.lodashjs.com/docs/lodash.mixin)

3.leecode刷题

- [二叉树的前序遍历](https://leetcode-cn.com/leetbook/read/data-structure-binary-tree/xeywh5/)
- [二叉树的中序遍历](https://leetcode-cn.com/leetbook/read/data-structure-binary-tree/xecaj6/)

4.项目进度

- [x] 分页器功能
- [x] 评论功能排版



## 2021.8.19
1.文章阅读
- [好的 Web 前端年薪会有多少？](https://www.zhihu.com/question/19723850/answer/1561992130)
- [JavaScript、jQuery、AJAX、JSON 这四个之间的关系？](https://www.zhihu.com/question/31305968/answer/116439739)

2.源码阅读
- [JavaScript学习笔记（二十七）-- ajax及ajax封装](https://zhuanlan.zhihu.com/p/114501873)
- [.stubArray](https://www.lodashjs.com/docs/lodash.stubArray)

3.leecode刷题

- [两数相除](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xwp6vi/)
- [快乐数](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xw99u7/)

4.项目进度

- [x] xxxxx

## 2021.8.18
1.文章阅读
- [Umijs前端微应用小试牛刀](https://zhuanlan.zhihu.com/p/90214542)
- [公司真实的项目——请求数据(umijs)](https://zhuanlan.zhihu.com/p/368505877)

2.源码阅读
- [精读《@umijs/use-request》源码](https://zhuanlan.zhihu.com/p/141673983)
- [.over](https://www.lodashjs.com/docs/lodash.over)

3.leecode刷题

- [移动零](https://leetcode-cn.com/leetbook/read/all-about-array/x9rh8e/)
- [颜色分类](https://leetcode-cn.com/leetbook/read/all-about-array/x9wv2h/)

4.项目进度

- [x] 关于页面排版
- [x] 评论功能
- [x] 分页器功能


## 2021.8.17
1.文章阅读
- [Ant Design Pro V5 已经支持预览](https://zhuanlan.zhihu.com/p/141740103)
- [项目实战-UmiJS开发](https://zhuanlan.zhihu.com/p/323458441)

2.源码阅读
- [.method](https://www.lodashjs.com/docs/lodash.method)
- [.toPath](https://www.lodashjs.com/docs/lodash.toPath)

3.leecode刷题

- [除自身以外数组的乘积](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xw8dz6/)
- [分割回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xdr7yg/)
- [正则表达式匹配](https://leetcode-cn.com/leetbook/read/top-interview-questions-hard/xd2egr/)

4.项目进度

- [x] 关于页面排版
- [x] 评论功能



## 2021.8.15
1.文章阅读
- [使用UmiJS框架开发React](https://zhuanlan.zhihu.com/p/364551179)
- [项目实战-UmiJS开发](https://zhuanlan.zhihu.com/p/323458441)

2.源码阅读
- [.lowerCase](https://www.lodashjs.com/docs/lodash.lowerCase)
- [.replace](https://www.lodashjs.com/docs/lodash.replace)

3.leecode刷题

- [三数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvpj16/)
- [两数相加](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvw73v/)
- [电话号码的字母组合](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xv8ka1/)

4.项目进度

- [x] 关于页面排版


## 2021.8.13
1.文章阅读
- [UmiJS](https://umijs.org/zh-CN/docs/getting-started)
- [阿里开源可插拔 React 跨端框架- Umi](https://zhuanlan.zhihu.com/p/68679617)

2.源码阅读
- [.random](https://www.lodashjs.com/docs/lodash.random)
- [.add](https://www.lodashjs.com/docs/lodash.add)

3.leecode刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
- [字符串的排序](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)
- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4.项目进度

- [x] XXXXXXXX


## 2021.8.12
1.文章阅读
- [初学者也能看懂的 Vue3 源码中那些实用的基础工具函数](https://juejin.cn/post/6994976281053888519)
- [阿里开源可插拔 React 跨端框架- Umi](https://zhuanlan.zhihu.com/p/68679617)

2.源码阅读
- [.every](https://www.lodashjs.com/docs/lodash.every)
- [.find](https://www.lodashjs.com/docs/lodash.find)

3.leecode刷题

- [寻找数组的中心](https://leetcode-cn.com/leetbook/read/array-and-string/yf47s/)
- [旋转矩阵](https://leetcode-cn.com/leetbook/read/array-and-string/clpgd/)
- [回文字符串](https://leetcode-cn.com/leetbook/read/array-and-string/conm7/)

4.项目进度

- [x] XXXXXXXXXX


## 2021.8.11
1.文章阅读
- [初学者也能看懂的 Vue3 源码中那些实用的基础工具函数](https://juejin.cn/post/6994976281053888519)

2.源码阅读
- [.findIndex](https://www.lodashjs.com/docs/lodash.findIndex)
- [.indexOf](https://www.lodashjs.com/docs/lodash.indexOf)

3.leecode刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- [删除排序数字中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4.项目进度
- [x] xxxxx
