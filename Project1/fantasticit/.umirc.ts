import { defineConfig } from 'umi';
// const px2rem = require('postcss-px2rem');
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 引入antd
  antd: {},
  // 引入dva
  dva: {
    immer: true,
    hmr: true,
  },
  locale: {},
 
  // 文件名加上hash
  hash:true,
  // 加入百度统计

    analytics: {
      ga: 'google analytics code',
      baidu: '8853402dbb3df12b6e2a548c11fdbd22'
    },

  // 按需加载
  dynamicImport: {
    loading: '@/component/Loading',
  },
    // 自适应http或https，上吊自愿的自愿的请求协议，以//开头
  scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],
 
  // 服务器上的子路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812B/wangzhaozhao/fantasticit/' : '/',
  // 服务器上的路有前缀
  // base:"/1812B/wangzhaozhao/fantasticit"
  base:process.env.NODE_ENV === 'production' ? '/1812B/wangzhaozhao/fantasticit': '/',
  
})
