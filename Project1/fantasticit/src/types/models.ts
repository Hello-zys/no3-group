import { ArticleModelState } from "@/models/article";
import { KnowModelState} from "@/models/knowledge"
import {AboutModelState } from '@/models/about'
import { MboardModelState } from "@/models/mboard";
import { FileModelState } from "@/models/file";
export interface IRootState{
    loading: any;
    language: any;
    article: ArticleModelState,
    knowledge:KnowModelState,
    mboard:MboardModelState,
    about:AboutModelState,
    commentnum: number | undefined,
    file:FileModelState,

}
    

