export interface IPosterItem{
    width: number;
    height: number;
    html: string;
    name: string;
    pageUrl: string;
}
