export interface ICommentItem {
    id: string;
    content: string;
    html: string;
    createAt: string;
    updateAt: string;
    length?:number,
    pop?:Function,
    push?:Function,
    concat?:Function,
    parentCommentId?:string;
    replyUserEmail?:string;
    replyUserName?:string;
    children?:ICommentItem[];
    email:string;
    hostId:string;
    name:string;
    pass:boolean;
    url:string;
    userAgent:string
  }
  export interface Comments{
    id?: string;
    createAt?: string;
    updateAt?: string;
    content?: string
    email?: string
    hostId?:string
    html?:string
    name?: string
    pass?: boolean
    url?: string
    userAgent?:string
    parentCommentId?:string| null;
    replyUserEmail?:string| null;
    replyUserName?:string| null;
  }