import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { Carousel } from 'antd';
import { useDispatch, useSelector, IRouteComponentProps } from 'umi';
import styles from './index.less'; // 等于启用了css-module
import Recommed from '@/component/recommed';
import Content from '@/component/content'
import classnames from 'classnames';
import Nav from '@/component/navlink'
import Tag from '@/component/tag'
import InfiniteScroll from 'react-infinite-scroll-component';

const Article: React.FC<IRouteComponentProps<{id:string}>> = (props) => {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);
  console.log(article.articleTitles, '++++++++++++++++++++++++++++++++++++++');
  // 获取文章页数据
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: {
        page,
        type: ''
      }
    })
  }, [page]);
  //  文章标题
  useEffect(() => {
    dispatch({
      type: 'article/getTitles'
    })
  }, []);
  // 获取右侧数据
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
  }, []);
  // 文章的tags
  useEffect(() => {
    dispatch({
      type: 'article/getTags'
    })
  }, []);

  // 跳详情
  const detail = (id: string) => {
    
    props.history.push(`/main/article/detail/${id}`)
  }
  // 跳tag详情页
  const tagDetail = (type: string) => {
    props.history.push(`/main/article/tag/${type}`)
  }

  // 无限滚动
  const pullupLoader = () => {
    setPage(page => page + 1)
  }

  return (
    <div className={classnames(styles.article)}>
      <div className={styles.left}>
        <div className={styles.left_top}>
          {/* 轮播图 */}
          <Carousel autoplay>
            <div className={styles.carousel_top}>
              <div>
                <p style={{ fontSize: '28px' }}>计算机动作原理</p>
                <p style={{ fontSize: '20px' }}>3个月前·557次阅读</p>
              </div>
            </div>
            <div className={styles.carousel_bottom}>
              <div>
                <p style={{ fontSize: '28px' }}>计算机动作原理</p>
                <p style={{ fontSize: '20px' }}>3个月前·557次阅读</p>
              </div>
            </div>
          </Carousel>
        </div>
        {/* 左边部分 */}
        <div className={styles.left_bottom}>
          <div className={styles.left_bottom_title}>
            {/* 标题头 */}
             <Nav></Nav>
          </div>
            <InfiniteScroll
              hasMore={article.articleCount >page * 12}
              loader={<h4>正在获取内容...</h4>}
              dataLength={article.articleList.length}
              next={pullupLoader}
            >
              {/* {文章内容} */}
                <Content detail={detail} />
          </InfiniteScroll>
        </div>
      </div>

      <div className={styles.right}>

        {/* 右上部分 */}
        <Recommed detail={detail}></Recommed>

        {/* 右下部分 */}
        <Tag tagDetail={tagDetail}></Tag>

      </div>
    </div>
  );
}

export default Article
