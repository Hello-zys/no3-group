import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import styles from './detail.less'; // 等于启用了css-module
import { useDispatch, useSelector, IRouteComponentProps} from 'umi';
import HightLight from '@/component/HighLight';
import { fomatTime } from '@/utils';
import { Pagination, Modal } from 'antd'
import ImageView from '@/component/ImageView';
import Recommed from '@/component/recommed';
import MboardInput from '@/component/mboardInput';

const detail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
    const [page, setPage] = useState(1)
    const dispatch = useDispatch();
    const state = useSelector((state: IRootState) => state);
    const article = useSelector((state: IRootState) => state.article);
    const [isModalVisible, setIsModalVisible] = useState(true)
    // const id = props.location.pathname.split('/')[3]
    const id = props.match.params.id;

    // console.log(JSON.parse(article.views.toc),'........................');

    useEffect(() => {
        dispatch({
            type: 'article/getViews',
            payload: id
        })
        dispatch({
            type: 'article/getRecommend',
            payload: id
        })
        dispatch({
            type: 'mboard/getMboard',
            payload: {
                page,
                hostId: id,
            }
        })
    }, [id, page]);

    const pagechange = (page: number) => {
        setPage(() => page = page)
    }

    // 跳详情
    const detail = (id: string) => {
        props.history.push(`/main/article/detail/${id}`)
    }

    // 滚动

    const onScroll = (id: string) => {
        document.getElementById(id)!.scrollIntoView({
            behavior: "smooth",//自带属性
            block: "start",
            inline: "nearest"
        })
    }

    // 确认框
    const handleOk =async () => {
        setIsModalVisible(false);
        let result = await fetch('http://127.0.0.1:7001/pay',{
            method:"POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({totalAmount:article.views.totalAmount,id:article.views.id})
        }).then(res=>res.json())
        if (result.url){
            window.location.href = result.url;
        }else{
            history.go(-1)
        }
    };

    // 取消框
    const handleCancel = () => {
        setIsModalVisible(false);
    };

    if (!Object.keys(article.views).length) {
        return null;
    }
    return (
        <ImageView>
            <div className={styles.detail}>
                <div className={styles.left}>
                    <div className={styles.left_content}>
                        {article.views.cover && <img src={article.views.cover} className={styles.imgs} />}
                        <p style={{ textAlign: "center", lineHeight: "50px" }}>
                            <h1 style={{ fontSize: '36px', fontWeight: 600 }}>{article.views.title}</h1>
                            <i>发布于{fomatTime(article.views.publishAt!)}•阅读量{article.views.views} </i>
                        </p>
                        <HightLight>
                            <div dangerouslySetInnerHTML={{ __html: article.views.html! }}></div>
                        </HightLight>
                    </div>
                    <div style={{ width: "100%", textAlign: 'center', lineHeight: "60px", fontSize: "22px" }}>评论</div>
                    {/* 评论 */}
                    <div className={styles.comment}>
                        <MboardInput></MboardInput>
                        <Pagination defaultCurrent={1} total={state.mboard.commentnum} pageSize={6} onChange={pagechange} />
                    </div>
                </div>


                <div className={styles.right}>
                    {/* 右上部分 */}
                    <Recommed detail={detail}></Recommed>

                    {/* 右下部分 */}
                    {
                        <div className={styles.right_bottom}>
                            <p>目录</p>
                            <div className={styles.right_content}>
                                {
                                    (JSON.parse(article.views.toc!)).map((item: any, index: number) => {
                                        return <li
                                            key={item.id}
                                            id={item.id}
                                            style={{
                                                paddingLeft: +item.level * 12 + "px",
                                                fontSize: 18 - (+item.level) + "px",
                                            }}
                                            onClick={() => onScroll(item.id)}
                                        >
                                            <span style={{ fontSize: 30 - (+item.level) * 2 + 'px' }}>·</span>
                                            {item.text}
                                        </li>
                                    })
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>
            {
                article.views.totalAmount && <Modal title="确认以下收费信息" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <p>支付金额:￥{article.views.totalAmount}</p>
                </Modal>
            }
        </ImageView>
    )
}

export default detail;

