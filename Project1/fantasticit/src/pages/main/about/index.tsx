import { useDispatch, useSelector } from "@/.umi/plugin-dva/exports";
import { IRootState } from "@/types";
import React, { useEffect, useState} from "react"
import styles from './index.less'; // 等于启用了css-module
import { Input, Modal, Pagination } from 'antd';
import moment from 'moment';
import "./index"
import {RouteComponentProps} from 'react-router-dom'

const className = require('classnames');
const { TextArea } = Input;
import { SmileFilled } from '@ant-design/icons'
import { Comment, Avatar } from 'antd';
import Content from '@/component/content'
const About: React.FC<RouteComponentProps> = props => {
    const [page, setPage] = useState(1)
    const dispatch = useDispatch();
    const [flag, setflag] = useState(false)
    const [id, setid] = useState(String)
    const article = useSelector((state: IRootState) => state);
    let uid = "a5e81ffe-0ad0-4be9-acca-c0462b1b98a1"

    useEffect(() => {  
        console.log(article.about.commentnum);      
    });

    useEffect(() => {
        dispatch({
            type: 'about/getAbout',
            payload: {
                page,
                id:uid
            }
        })
    }, [page]);
 
    // 弹框
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [names, setnames] = useState('');
    const [email, setemail] = useState('');
    const [ids, setids] = useState('');

    const showModal = () => {
        const user = localStorage.user ? JSON.parse(localStorage.user) : "";
        if (!user) {
            setIsModalVisible(true);
        }
    };

    const handleOk = () => {
        localStorage.user = JSON.stringify({ names, email })
        setIsModalVisible(false);

    };


    // 页码更改
    const pagechange = (page: number) => {
        setPage(page)
    }

    const SPublish=()=>{
        console.log(123456);
    }
    const detail = (id: string) => {
        props.history.push(`/main/article/detail/${id}`)
    }
    return <div className={styles.aboutcss}>
        <div className={styles.container}>
            <div className={styles.mainimg} >
                <img src=" https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" alt="文章封面" />
            </div>
            <div className={styles.titheader}>
                <div className={styles.markdown}>
                    <h2 className={styles.tith2}>这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。</h2>
                </div>
            </div>
        </div>
      
     
        <div className={styles.maincsses}>
            <div className={styles.containercss}>
                <p >评论</p>
            </div>
            <div className={styles.CommentBox}>
                <div className={styles.textarea} onClick={showModal}>
                    <TextArea rows={4} placeholder="请输入评论内容（支持 Markdown）" />
                    <p className={styles.titpcss}> <span> <SmileFilled className={styles.smiling} />表情</span>
                        <button className={styles.sendbtn}>发布</button></p>
                </div>
                {/* style={{ color: "blue", fontSize: "20px" }}  */}
                <div className={styles.contentcss}>
                    {
                        article.about.about.map((item:any,index:number)=>{
                            return   <Comment
                            className={styles.antcomment}
                            actions={[<span key={item.id}>{item.userAgent}</span>,
                            <span>大约 {moment(item.createAt).format("ddd, hA")}</span>,
                            <span onClick={() => {
                                setflag(!flag)
                                setid(item.id)
                            }}> {item.children?.length ? "" : "回复"}  </span>
                            ]}
                            author={<a>{item.name}</a>}
                            avatar={
                                <Avatar style={{
                                    background: item.name === "Fan" ? "skyblue" : ""
                                                || item.name === "小可爱" ? "skyblue" : ""
                                                || item.name === "达到" ? "red" : ""
                                                || item.name === "lucux" ? "red" : ""
                                                || item.name === "berryMax" ? "redpink" : ""
                                                || item.name === "zhaoxu" ? "redpink" : ""
                                                || item.name === "mr-xu" ? "yellow" : ""
                                                ||"pink"
                                }} size="large">{item.name}</Avatar>}
                                content={ <p dangerouslySetInnerHTML={{ __html: item.content }} key={item.id}></p>}
                            key={item.id}>
                                {//子数据渲染
                                item.children?.map((it: { id: React.Key | null | undefined; userAgent: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; createAt: moment.MomentInput; name: {} | null | undefined; content: any; }) => {
                                    return <Comment
                                        actions={[<span key={it.id}>{it.userAgent}</span>,
                                        <span>大约 {moment(it.createAt).format("ddd, hA")}</span>,
                                        <span onClick={() => {
                                            setflag(!flag)
                                            setid(item.id)
                                        }}> 回复  </span>
                                        ]}
                                        author={<a>{it.name}</a>}
                                        avatar={
                                            <Avatar style={{
                                                background: item.name === "Fan" ? "skyblue" : ""
                                                || item.name === "小可爱" ? "skyblue" : ""
                                                || item.name === "达到" ? "red" : ""
                                                || item.name === "lucux" ? "red" : ""
                                                || item.name === "berryMax" ? "redpink" : ""
                                                || item.name === "zhaoxu" ? "redpink" : ""
                                                || item.name === "mr-xu" ? "yellow" : ""||"pink"
                                            }} size="large">{it.name}</Avatar>}
    
                                            content={ <p dangerouslySetInnerHTML={{__html:it.content }}></p>}
                                        key={it.id}/>
                                })
                            }
                            {  //第二层文本框
                                item.id === id && flag ? <div className="textarea" onClick={showModal}>
                                    <TextArea rows={4} placeholder="回复custw" />
                                    <p>
                                        <span> <SmileFilled style={{ color: "blue", fontSize: "20px" }}/>表情</span>
                                        <span><button onClick={() => setflag(false)}>收起</button> <button onClick={SPublish}>发布</button></span>
                                    </p>
                                </div> : null
                            }
                        </Comment>
                        })
                    }
                <div className={styles.changenum}> <Pagination defaultCurrent={1} total={article.about.commentnum} pageSize={6} onChange={pagechange} /></div>
                </div>
            </div>
        </div>
        <div>
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={() => setIsModalVisible(false)}>
                <p>昵称* <Input placeholder="输入昵称" value={names} onChange={(e) => setnames(e.target.value)} /></p>
                <p>邮箱* <Input placeholder="输入邮箱" value={email} onChange={(e) => setemail(e.target.value)} /></p>
            </Modal>
        </div>
        <Content detail={detail} />

    </div>
}
export default About