import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
import moment from '../../../component/moment'
// import { Carousel } from 'antd';
import './index.less'
import { useDispatch, useSelector } from 'umi';
const File: React.FC<RouteComponentProps> = (props) => {
    const dispatch = useDispatch();
    const archives = useSelector((state: IRootState) => state.file.arrList);
    const recommends = useSelector((state: IRootState) => state.article);
    const classlist = useSelector((state: IRootState) => state.file.classList);
    console.log(classlist);
    // 归档数据
    useEffect(() => {
        dispatch({
            type: 'file/gitFile'
        })
    }, []);
    // 分类数据
    useEffect(() => {
        dispatch({
            type: 'file/gitclass'
        })
    }, []);
    // 获取推荐读
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
    }, []);
    const detalis = (id: string) => {
        props.history.push(`/main/article/detail/${id}`)
    }
    const hidLink = (value?: string) => {
        props.history.push(`/main/article/tag/${value}`)
    }
    let totalNun = 0;
    for (let key in archives) {
        for (let item in archives[key]) {
            totalNun += archives[key][item].length;
        }
    }
    return (
        <div className='file'>

            <div className='content'>
                <div className='left'>
                    <div className="left_top">
                        <h3>归档</h3>
                        <p>共计<span>{totalNun}</span>篇</p>
                    </div>
                    {
                        <ul>
                            <li>
                                {
                                    Object.keys(archives).sort((a, b) => {
                                        return Number(b) - Number(a)
                                    }).map((val, index) => {
                                        return <div key={index} className='fileitem'>
                                            <h1 className="file_items">{val}</h1>
                                            {
                                                Object.keys((archives[Number(val)])).map((item, index1) => {
                                                    return (
                                                        <div key={index1} className='file_name'>
                                                            <h2 className="h2s">{item}</h2>
                                                            <ul>
                                                                {
                                                                    (archives as any)[val][item].map((item1: any, index2: number) => {
                                                                        return (
                                                                            <div key={index2}>
                                                                                <li style={{ listStyle: "disc" }} onClick={() => detalis(item1.id)}
                                                                                    className="body_san"
                                                                                >
                                                                                    <span>
                                                                                        {''}
                                                                                        {item1.createAt.slice(5, 10)}
                                                                                    </span>
                                                                                    <span className="spans">{item1.title}</span>
                                                                                </li>
                                                                            </div>
                                                                        )
                                                                    })
                                                                }
                                                            </ul>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    })
                                }
                            </li>
                        </ul>
                    }
                </div>
                <div className="right">
                    <div className="right_top">
                        <div className='right_top_tit'>
                            <h3 style={{ marginLeft: 20, }}><b>推荐阅读</b></h3>
                        </div>
                        <div className="right_top_body">
                            {
                                recommends.recommend.map((item, index) => {
                                    return (
                                        <div key={index} className="item_top" onClick={() => detalis(item.id)}>
                                            <span>{item.title}</span> ·
                                            <span>{moment(item.createAt).fromNow()}</span>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <div className="right_botten">
                        <div className="right_bottom_tex">
                            <h3 style={{ marginLeft: 20 }}><b>文章分类</b></h3>
                        </div>
                        <div className="right_bottom_body">
                            {
                                classlist.map((item, index) => {
                                    return <div key={index} className="item_text"
                                        onClick={() => { hidLink(item.value) }}
                                    >
                                        <span>{item.label}</span>
                                        <p>共计{item.articleCount}篇文章</p>
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default File