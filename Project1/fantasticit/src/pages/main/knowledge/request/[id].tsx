import {IRootState} from '@/types'
import {useEffect,useMemo,useState} from 'react';
import styles from './request.less';
const classNames = require('classnames');
import {IRouteComponentProps,useDispatch,useSelector} from 'umi'
import MboardInput from '@/component/mboardInput'
import moment from '@/component/moment'
import Toolbar from '@/component/toolbar'
const request:React.FC<IRouteComponentProps<{id:string}>>=(props)=>{
    const dispatch=useDispatch();
    const knowledge=useSelector((state:IRootState)=>state.knowledge)
    
    const obj=knowledge.views;
    let  obb=props.match.params.id.split(',');
    let parentId=obb[1];
    let id=obb[0];
    
    useEffect(() => {
        dispatch({
            type: 'knowledge/getDetail',
            payload:id
          })
        dispatch({
            type:'knowledge/getContent',
            payload:parentId
        })
      }, [id,parentId]);

      const getClick=(id:string,parentId:string)=>{
        let childId=id;
        props.history.push(`/main/knowledge/request/${parentId},${childId}`)
      }

    return (
        <div className={styles.known}>
            <div className={styles.left}>
               <h1>{obj.title}</h1>
               <p className={styles.yuedu}>发布于<span>{moment(obj.updateAt).fromNow()}·阅读量<span>{obj.views}</span></span></p>
               <div
                          dangerouslySetInnerHTML={{
                            __html: obj.html!,
                          }}
                        ></div>
                <p className={styles.weibu}>发布于<span>{obj.updateAt}|<span>版权信息：非商用-署名-自由转载</span></span></p>
                <div className={styles.skip} >
                  {
                     knowledge.knowledgedetail.children.map((item: { order: number; title: {} | null | undefined; id: string; parentId: string; })=>{
                      if(item.order===knowledge.views.order-1){
                       return <span className={styles.prev} onClick={()=>getClick(item.id,item.parentId)}>&lt;{item.title}</span>
                        
                      }else if(item.order===knowledge.views.order+1){
                        return <span onClick={()=>getClick(item.id,item.parentId)} className={styles.next} >{item.title}&gt;</span>
                      }
                    })
                  }
            </div>
            <div>
              <h3>评论</h3>
              <MboardInput></MboardInput>
              <Toolbar id={useMemo(() => id, [id])}></Toolbar>
            </div>
            </div>
            <div className={styles.right}>
            <div className={styles.right_top}>
          <h4>Linux/Unix 编程思想</h4>
          <div>
            {
              knowledge.knowledgedetail.children?knowledge.knowledgedetail.children.map((item,index)=>{
                return <p key={index} className={styles.programme} onClick={()=>getClick(item.id,item.parentId)}>
                  <span className={knowledge.views.order===index?styles.active:''}>{item.title}</span>
                </p>
              }):''
            }
          </div>
          
        </div>
        <div className={styles.right_bottom}>
            <h4>目录</h4>
            <div className={styles.uld}>
            {
              knowledge.views.toc?(JSON.parse(knowledge.views.toc!)).map((item:any, index:number) => {
                return <>
                  {
                    item.level==="2"&&<li  style={{fontSize:"16px",listStyle:"disc"}}>{item.text}</li>
                  }
                  {
                    item.level==="3"&&<li style={{paddingLeft:"10px",listStyle:"disc",fontSize:"14px"}}>{item.text}</li>
                  }
                  {
                    item.level==="4"&&<li style={{paddingLeft:"20px",listStyle:"disc",fontSize:"13px"}}>{item.text}</li>
                  }
                      </>
                }):''
            }
            </div>
        </div>
            </div>
        </div>
    )
}

export default request;
