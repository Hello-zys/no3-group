import { IAboutItem } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getAbout } from '@/services';

export interface AboutModelState {
  [x: string]: any;
  about: IAboutItem[],
  commentnum: number,
  
}

export interface AboutModelType {
  namespace: 'about';
  state: AboutModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getAbout: Effect
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<AboutModelState>;
  };
}

const AboutModel: AboutModelType = {
  namespace: 'about',
  state: {
    about: [],
    commentnum: 0
  },

  effects: {
    *getAbout({ payload }, { call, put }) {
      console.log(123455467566666666666666666666666666666);

      let result = yield call(getAbout, payload.page,payload.id);
      console.log('result333333333333333------------', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            about: result.data[0],
            commentnum: result.data[1]
          }

        })
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default AboutModel;