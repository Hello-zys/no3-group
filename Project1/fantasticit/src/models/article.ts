import { IArticleItem, IRootState } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getArticleList, getCatalog, getRecommend, getTags, getTitles, getViews } from '@/services';

export interface ArticleModelState {
  articleTags: IArticleItem[];
  recommend: IArticleItem[];
  articleList: IArticleItem[];
  articleTitles: IArticleItem[];
  views: Partial<IArticleViews>;
  catalog: IArticleItem[];
  articleTag: IArticleItem[];
  articleTagCount: number,
  articleCount: number;
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    getTags: Effect;
    getTitles: Effect;
    getViews: Effect;
    getCatalog: Effect;
    getArticleTag: Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const ArticleModel: ArticleModelType = {
  namespace: 'article',
  state: {
    recommend: [],
    articleList: [],
    articleTitles: [],
    articleTags: [],
    articleTag: [],
    views: {} as IArticleItem,
    catalog: [],
    articleTagCount: 0,
    articleCount: 0,
  },

  effects: {

    //  Tag数据
    *getArticleTag({ payload }, { call, put, select }) {
      let result = yield call(getArticleList, payload.page, payload.type);
      let articleTag = yield select((state: IRootState) => state.article.articleTag);
      if (result.statusCode === 200) {
        articleTag = payload.page === 1 ? result.data[0] : [...articleTag, ...result.data[0]]
        yield put({
          type: 'save',
          payload: {
            articleTag,
            articleTagCount: result.data[1]
          }
        })
      }
    },

    // 目录
    *getCatalog({ payload }, { call, put }) {
      let result = yield call(getCatalog, payload);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { catalog: result.data }
        })
      }
    },

    // views数据
    *getViews({ payload }, { call, put }) {

      let result = yield call(getViews, payload);

      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { views: result.data }
        })
      }
    },

    // 推荐阅读数据
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend, payload);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        })
      }
    },

    // 文章内容
    *getArticleList({ payload }, { call, put, select }) {
      let result = yield call(getArticleList, payload.page, payload.type);
      let articleList = yield select((state: IRootState) => state.article.articleList);
      console.log(articleList)
      console.log('result2', result);
      if (result.statusCode === 200) {
        articleList = payload.page === 1 ? result.data[0] : [...articleList, ...result.data[0]]
        yield put({
          type: 'save',
          payload: {
            articleList,
            articleCount: result.data[1]
          }
        })
      }
    },

    // 文章标签
    *getTags({ payload }, { call, put }) {
      let result = yield call(getTags);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleTags: result.data,
          }
        })
      }
    },

    // 文章分类数据
    *getTitles({ payload }, { call, put }) {
      let result = yield call(getTitles);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleTitles: result.data,
          }
        })
      }
    },

  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default ArticleModel;