import { IArticleItem,Category,IArticleComment,IArticleDetail,IRootState} from '@/types';
import { Effect,  Reducer } from 'umi';
import { gitFile , gitclass,getArticleDetail,getArticleComment} from '@/services';
export interface FileModelState {
    arrList: {[key:string]:{[key:string]:IArticleItem[]}}
    classList: Category[]
    articleComment: IArticleComment [];
    articleCommentCount: number;
    articleDetail: Partial<IArticleDetail>;
  
}
export interface FileModelType {
    namespace: 'file';
    state: FileModelState;
    // 异步action，相当于vuex中的action
    effects: {
        gitFile: Effect
        gitclass: Effect
        getArticleDetail: Effect;
        getArticleComment: Effect;
    

    };
    // 同步action，相当于vuex中的mutation
    reducers: {
        save: Reducer<FileModelState>;
    };
}
const FileModel: FileModelType = {
    namespace: 'file',
    state: {
        arrList: {},
        classList:[],
        articleComment: [],
        articleCommentCount: 0,
        articleDetail: {}
    
    },
    effects: {
        *gitFile({ payload }, { call, put }) {
            let result = yield call(gitFile);
         
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: {
                        arrList: result.data,
                    }
                })
            }
        },
        *gitclass({ payload }, { call, put }) {
            let result = yield call(gitclass);
            console.log('result ssssssssssssssssssssssssssssssss', result);
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: {
                        classList: result.data,
                    }
                })
            }
        },
        *getArticleDetail({payload}, { call, put }) {
            let result = yield call(getArticleDetail, payload);
            console.log('result...', result);
            if (result.success === true) {
              yield put({
                type: 'save',
                payload: {
                  articleDetail: result.data
                }
              })
            }
          },
          *getArticleComment({payload}, { call, put, select }) {
            let preArticleComment = yield select((state:IRootState)=>state.file.articleComment);
            let result = yield call(getArticleComment, payload.id, payload.page);
            console.log('result...', result);
            if (result.success === true) {
              let articleComment = result.data[0];
              if (payload.page !== 1){
                articleComment = [...preArticleComment, ...articleComment];
              }
              yield put({
                type: 'save',
                payload: {
                  articleComment,
                  articleCommentCount: result.data[1]
                }
              })
            }
          },
      
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    }
};

export default FileModel;