import { IKnowItem,Category,IKnowledgeItem,IKnowTable } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getKnowList,getCategory,getDetail,getContent,getLike} from '@/services';

export interface KnowModelState {
    knowList: IKnowItem[];
    knowcategory:Category[];
    knowledgedetail:IKnowledgeItem;
    views:IKnowTable;
}

export interface KnowModelType {
  namespace: 'knowledge';
  state: KnowModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getKnowList: Effect;
    getCategory:Effect;
    getDetail:Effect;
    getContent:Effect;
    getLike: Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<KnowModelState>;
  };
}

const KnowModel: KnowModelType = {
  namespace: 'knowledge',
  state: {
    knowList: [],
    knowcategory:[],
    knowledgedetail:{} as IKnowledgeItem,
    views:{} as IKnowTable,
  },

   effects: {
    // 左侧内容
    *getKnowList({ payload }, { call, put }) {
      let result = yield call(getKnowList, payload);
      console.log('result--------------------------------------------', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            knowList: result.data[0],
            // articleCount: result.data[1]
          }
        })
      }
    },
       // 详情页
      *getDetail({ payload }, { call, put }) {
        
        let result = yield call(getDetail,payload);
        console.log('result159511549655', result);  
        if (result.success === true) {
          yield put({
            type: 'save',
            payload: { knowledgedetail: result.data }
          })
        }
      },

          // 二级详情页数据
    *getContent({ payload }, { call, put }) {

      let result = yield call(getContent, payload);
      console.log('xxxxxxxxxxxxxxx',result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { views: result.data }
        })
      }
    },


   // 右侧下部分
  *getCategory({ payload }, { call, put }) {
    let result = yield call(getCategory);
    console.log('result1111111111111111', result);
    if (result.success === true) {
      yield put({
        type: 'save',
        payload: {
          knowcategory: result.data,
        }
      })
    }
  },
  
  *getLike({ payload }, { call, put }) {
    let result = yield call(getLike, payload.id, payload.type)
    if (result.statusCode === 200) {
      yield put({
        type: "save",
        payload: { views: result.data }
      })
    }
  }
},


  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default KnowModel;