import React, { FC } from 'react'
import { useSelector, useDispatch } from 'umi'

import { Input, Comment, Avatar, Modal, Pagination } from 'antd';
import { useEffect, useState } from 'react';
import { SmileFilled } from '@ant-design/icons'
import './index.less'
import moment from 'moment';
import { IRootState, ICommentItem, Comments } from '@/types'
import { getComment } from '@/services';
const { TextArea } = Input;

const MboardInput: FC = () => {
    const [page, setPage] = useState(1)
    const [flag, setflag] = useState(false)
    const [id, setid] = useState(String)
    const [text, settext] = useState('')
    const [obj, setobj] = useState(Object)
    const comment = useSelector((state: IRootState) => state.mboard);
    // 弹框
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [names, setnames] = useState('');
    const [email, setemail] = useState('');
    const [ids, setids] = useState('');
    // 判断是否登录
    const showModal = () => {
        const user = localStorage.user ? JSON.parse(localStorage.user) : "";
        if (!user) {
            setIsModalVisible(true);
        }
    };
    // 关闭弹框
    const handleOk = () => {
        localStorage.user = JSON.stringify({ names, email })
        setIsModalVisible(false);
    };


    // 第一次发布形成的数据
    const changeFile = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        settext(e.target.value)
        const user = JSON.parse(localStorage.user)
        setobj({
            content: text,
            email: user.email,
            hostId: +new Date() + "",
            html: `<p>${text}</p>`,
            name: user.names,
            pass: false,
            url: '/main/guestbook',
            userAgent: "QQBrowser 10.8.4455.400 Blink 70.0.3538.25 Windows 10"
        })
    }
    // 第一层
    const Fpublish = () => {
        getComment(obj)
        settext('')
    }

    // 第二层发布
    const SPublish = (id: string) => {        
        function fn(id: string) {
            let item = comment.mboardcomment.filter(item =>{
                if(item.children?.length){
                    return item.children?.filter(it=>it.id===id)[0]
                }else{
                    return item.id===id
                }
            })[0]
            const user = JSON.parse(localStorage.user)
            setobj({
                content: text,
                email: user.email,
                hostId: "8d8b6492-32e5-44e5-b38b-9a479d1a94bd",
                html: `<p>${text}</p>↵`,
                name: user.names,
                parentCommentId: "c751c340-366f-4d84-89d9-58d69c00670a",
                pass: false,
                replyUserEmail: item.email,
                replyUserName: item.name,
                url: "/main/guestbook",
                userAgent: "QQBrowser 10.8.4455.400 Blink 70.0.3538.25 Windows 10",
                id: +new Date() + "",
            })
        }
        fn(id)
        getComment(obj)
        settext('')
        setflag(false)
    }
    // 回调函数
    let name = (mboardcomment: ICommentItem[]) => {
        return mboardcomment?.map(item => {
            return <div className="div">
                <Comment
                    actions={[<span key={item.id}>{item.userAgent}</span>,
                    <span>大约 {moment(item.createAt).format("ddd, hA")}</span>,
                    <span onClick={() => {
                        setflag(!flag)
                        setid(item.id)
                    }}> {item.children?.length ? "" : "回复"}  </span>
                    ]}
                    author={<a>{item.name}</a>}
                    avatar={
                        <Avatar style={{
                            background:item.name === "周" ? "slateblue" : ""|| item.name === "123" ? "red" : ""||"tan"
                        }} size="large">{item.name}</Avatar>}
                    content={<p dangerouslySetInnerHTML={{ __html: item.content }}></p>}
                    key={item.id}
                >
                    {item.children?.length ? name(item.children) : null}
                </Comment>
                {
                    item.id === id && flag ? <div className="textarea" onClick={showModal}>
                        <TextArea rows={4} placeholder="回复custw" value={text} onChange={e => settext(e.target.value)} />
                        <p>
                            <span> <SmileFilled style={{ color: "blue", fontSize: "20px" }} />表情</span>
                            <span><button onClick={() => setflag(false)}>收起</button> <button onClick={() => SPublish(item.id)}>发布</button></span>
                        </p>
                    </div> : null
                }
            </div>

        })
    }


    return (
        <div className="comment">
            <div className="textarea" onClick={showModal}>
                <TextArea rows={4} placeholder="请输入评论内容（支持 Markdown)" value={text} onChange={e => changeFile(e)} />
                <p> <span> <SmileFilled style={{ color: "blue", fontSize: "20px" }} />表情</span>     <button onClick={Fpublish}>发布</button></p>
            </div>

            {
                name(comment.mboardcomment)
            }

            {/* 弹框 */}
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={() => setIsModalVisible(false)}>
                <p>昵称* <Input placeholder="输入昵称" value={names} onChange={(e) => setnames(e.target.value)} /></p>
                <p>邮箱* <Input placeholder="输入邮箱" value={email} onChange={(e) => setemail(e.target.value)} /></p>
            </Modal>
        </div>
    )
}
export default MboardInput