import React ,{FC} from 'react'
import styles from "./index.less"

import { Spin} from 'antd'
const Loading:FC =()=>{
    return <>
    <div className={styles.loading}><Spin size="large"/></div>
    </>
}
export default Loading
