import { useSelector } from '@/.umi/plugin-dva/exports'
import { IRouteComponentProps } from 'umi';
import { IArticleItem, IRootState } from '@/types'
import React, { useState } from 'react'
import styles from './index.less'
interface Props{
    detail(id:string):void,
    handleSearch():void
}
const index: React.FC<Props> = (props) => {
    const [val,setVal] = useState('')
    const [list,setList] = useState([] as IArticleItem[])
    const articleList = useSelector((state:IRootState) => state.article.articleList)
    console.log(articleList);
     
    const changeVal=(e:React.ChangeEvent<HTMLInputElement>)=>{
       setVal(e.target.value)
       const arr = articleList.filter(item=>item.title.includes(e.target.value))
       setList(arr)  
    }

    return (
        <div className={styles.search}>
            <div>
            <header className={styles.header}><span className={styles._3jmZKBfWYIlO69xDtpXPz_}>文章搜索</span><span className={styles._2qpDZJMLbJxyMk}><span role="img" aria-label="close" className={styles.anticon}><svg
            onClick={()=>props.handleSearch()}
            viewBox="64 64 896 896" focusable="false" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path></svg></span><span style={{color: "var(--disable-text-color)"}}>esc</span></span></header>
            <input placeholder="输入关键字，搜索文章" className={styles.input} type="text" value={val}
            onChange={changeVal}
            ></input>
            <ul className={styles.ul}>
                {
                    list.map(item=>{
                        return <li
                        onClick={()=>props.detail(item.id)}
                        key={item.id}
                        >{item.title}</li>
                    })
                }
            </ul>
            </div>
        </div>
    )
}

export default index;
