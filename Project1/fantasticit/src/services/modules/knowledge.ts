import { request } from 'umi';
// 获取文章列表
export function getKnowList(page: number, pageSize = 6, status = 'publish'){
    return request('/api/knowledge', {
        // 查询知识小册主页面的参数
        params: {
            page,
            pageSize,
            status,      
        },
        // 表示请求体里面传递的数据
        data: {},
    })
}


// 文章页的右侧下方请求

export function getCategory(){
    return request(`/api/category?articleStatus=publish`);
}
// 详情页数据获取
export function getDetail(id:string){
    return request(`/api/knowledge/${id}`)
}
// 二层详情页数据获取
export function getContent(id:string){
    return request(`/api/knowledge/${id}/views`,{
        method:"post"
    });
}

export function getLike(id:string,type:string){
    return request(`/api/knowledge/${id}/likes`, {
        method:'POST',
        data:{
            type:type
        }
    })
}
