import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getGitRecomment, getGitDetail, getGitTag, getGitArtical } from "@/services";
import { IGitRecommendItem, IGitDetailItem, IGitTagItem, IGitArticalItem } from "@/types"


export interface IndexModelState {
  recommend: IGitRecommendItem[],
  detail: [IGitDetailItem[], number],
  tag: IGitTagItem[],
  artical: IGitArticalItem[]
}

export interface IndexModelType {
  namespace: 'git';
  state: IndexModelState;
  effects: {
    getGitRecomment: Effect;
    getGitDetail: Effect;
    getGitTag: Effect;
    getGitArtical: Effect;
  };
  reducers: {
    save: Reducer<IndexModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  }
}

const IndexModel: IndexModelType = {
  namespace: 'git',
  state: {
    recommend: [],
    detail: [[], 0],
    tag: [],
    artical: []
  },

  effects: {
    *getGitRecomment({ payload }, { call, put }) {
      let result = yield call(getGitRecomment);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        });
      }
    },
    *getGitDetail({ payload }, { call, put }) {
      let result = yield call(getGitDetail, payload.tag);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { detail: result.data }
        });
      }
    },
    *getGitTag({ payload }, { call, put }) {
      let result = yield call(getGitTag);
      console.log(result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { tag: result.data }
        });
      }
    },
    *getGitArtical({ payload }, { call, put }) {
      let result = yield call(getGitArtical);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { artical: result.data }
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  }
};

export default IndexModel;