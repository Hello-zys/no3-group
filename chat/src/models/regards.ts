import { IRootState, IAegardsItem, IRegardsState, Child } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getregardsList } from "@/services"
export interface regardsModelState {
  regardsList: IAegardsItem[],
  regardsListCount: number
}

export interface regardsModelType {
  namespace: 'regards';
  state: regardsModelState;
  effects: {
    getregardsList: Effect;
    changeregardsList: Effect
  };
  reducers: {
    save: Reducer<regardsModelState>;
    changesave:Reducer<regardsModelState>
  };
}

const IndexModel: regardsModelType = {
  namespace: 'regards',
  state: {
    regardsList: [],
    regardsListCount: 0
  },

  effects: {
    *getregardsList({ payload }, { call, put }) {
      let result = yield call(getregardsList, { ...payload });
      console.log('result...', result);
      if (result.statusCode === 200) {
        yield put({
          type: "save",
          payload: { regardsList: result.data[0], regardsListCount: result.data[1] }
        })
      }
    },
    *changeregardsList({ payload }, { put, select }) {
      let  regardsList  = yield select((state: IRegardsState) => state.regards.regardsList);
      if(payload.parentInd === "0"){
        let ind = regardsList.findIndex((item: IAegardsItem) => { return item.id === payload.id });
              yield put({
                type: 'changesave',
                payload: { ind ,indd:-1}
              })
      }else{
        let ind=regardsList.findIndex((item: IAegardsItem) => { return item.id === payload.parentInd });
        let indd=regardsList[ind].children.findIndex((item:IAegardsItem)=>{return item.id===payload.id});
        yield put({
          type:"changesave",
          payload:{
            ind,indd
          }
        })
      }
    }
  },


  reducers: {
    save(state, action) {
      let change=(arr:IAegardsItem[] | Child[])=>{
        arr.forEach((item:IAegardsItem)=>{
          item["gol"]=true;
          if(item.children?.length>0){
            change(item.children)
          }
        })
      }
      change(action.payload.regardsList)
      return {
        ...state,
        ...{regardsList:action.payload.regardsList,regardsListCount:action.payload.regardsListCount}
      };
    },
    changesave(state,action){
      let arr=JSON.parse(JSON.stringify(state?.regardsList));
      console.log(action.payload,"这里是payload");
      
      if(action.payload.indd<0){
        arr[action.payload.ind].pass=!arr[action.payload.ind].pass;
      }else{
        arr[action.payload.ind].children[action.payload.indd].pass=!arr[action.payload.ind].children[action.payload.indd].pass
      }
      return {
        ...state,
        regardsList:arr
      }
    },
  }
};

export default IndexModel;