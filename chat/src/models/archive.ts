import { Effect, ImmerReducer, Reducer} from 'umi';
import {getArchiveList,getCategory,getDeatil,getDetailRecommend} from "@/services"
import { ArchiveDetailItem, DeatailRecommendItem, Gostate,IArchiveStateItem } from '@/types/archive';
// import { IArchiveItem } from '@/types';

// 数据项类型
interface IArrItem {
  level: string;
  id: string;
  text: string;
}

export interface ArchiveModelState {
    archiveList: Partial<IArchiveStateItem>
    CategoryList:Gostate[],
    DetailItem:Partial<ArchiveDetailItem>,
    DeatilRecommendList:DeatailRecommendItem[],//详情数据推荐阅读  
    toc:IArrItem[]
}

export interface ArchiveModelType {
  namespace: 'archive';
  state: ArchiveModelState;
  effects: {
    getArchiveList: Effect;
    getCategory:Effect;
    getDeatil:Effect;
    getDetailRecommend:Effect
  };
  reducers: {
    save: Reducer<ArchiveModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const ArchiveModel: ArchiveModelType = {
  namespace: 'archive',

  state: {
    archiveList:{},
    CategoryList:[],
    DetailItem:{},//详情数据
    DeatilRecommendList:[],//详情推荐阅读数据
    toc:[]
  },

  effects: {
    *getArchiveList({ payload }, { call, put }) {
        let result = yield call(getArchiveList);
        if (result.statusCode === 200) {
          yield put({
            type: 'save',
            payload: { archiveList: result.data }
          })
        }
      },
      
    //   文章阅读数据
      *getCategory({ payload }, { call, put }) {
        let result = yield call(getCategory);
        if (result.statusCode === 200) {
          yield put({
            type: 'save',
            payload: { CategoryList: result.data }
          })
        }
      },
      // 列表详情数据
     
      * getDeatil({ payload }, { call, put }) {
        let result = yield call( getDeatil,payload);
        if (result.statusCode === 200) {
          yield put({
            type: 'save',
            payload: { DetailItem: result.data , toc:eval(result.data.toc)}
          })
        }
      },
      //详情列表推荐数据
      * getDetailRecommend({ payload }, { call, put }) {
      let result = yield call(getDetailRecommend,payload);
      console.log('result...', result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {DeatilRecommendList:result.data }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },

};

export default ArchiveModel;