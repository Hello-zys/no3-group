import React, { useState } from "react"
import styles from "./index.less"
import { NavLink } from "react-router-dom"
import { useSelector } from "react-redux"
import { IRootState } from "@/types"
import { useDispatch } from "umi"

const Route: React.FC = () => {
  let [list, setList] = useState([{ value: "", name: "所有", to: "/" }, { value: "fe", name: "前端", to: "/category/fe" }, { value: "be", name: "后端", to: "/category/be" }, { value: "reading", name: "阅读", to: "/category/reading" }, { value: "linux", name: "Linux", to: "/category/linux" }, { value: "leetcode", name: "Leetcode", to: "/category/leetcode" }, { value: "news", name: "要闻", to: "/category/news" }])
  return <div className={styles.route}>
    {
      list.map((item, index) => {
        return <NavLink to={item.to} className={index === Number(localStorage.getItem("ind")) ? styles.active : ""} onClick={() => localStorage.setItem("ind", index + "")} key={index}>{item.name}</NavLink >
      })
    }
  </div>
}

export default Route