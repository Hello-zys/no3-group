import React, { useEffect, useState } from 'react';
import Style from './index.less';
import classnames from 'classnames';

// 接收参数
interface IProps {
  toc: IArrItem[];
}
// 数据项类型
interface IArrItem {
  level: string;
  id: string;
  text: string;
}

const Linkage: React.FC<IProps> = (props) => {
  // 获取到的数组
  // 获取到的高亮
  const [ind, setInd] = useState('');
  let html: any[] = [];
  useEffect(() => {
    //   找到要高亮的标签
    setInd(props.toc[0]?.id)
    html = [
      ...document.querySelectorAll(
        `section h1,section h2,section h3,section h4,section h5,section h6`,
      ),
    ];
    // 添加滚动事件
  }, [props.toc]);
  useEffect(() => {
    document
      .querySelector('#root')
      ?.lastElementChild?.addEventListener('scroll', (e: any) => {
        html.forEach((ite) => {
          if (e.target.scrollTop > (ite as HTMLElement).offsetTop - 64) {
            ite.id && setInd(ite.id);
          }
        });
      });
  }, [html]);
  //   去到对应的位置
  const goHear = (id: string) => {
    setInd(id);
    document.getElementById(id)!.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  };
  return (
    <div style={{ width: '100%' }}>
      <p>
        <b>目录</b>
      </p>
      <ul className={Style.box}>
        {props.toc?.map((item) => {
          return (
            <li
              key={item.id}
              id={item.id}
              style={{
                paddingLeft: +item.level * 12 + 'px',
                fontSize: 18 - +item.level + 'px',
              }}
              className={classnames(
                Style.static,
                ind === item.id ? Style.active : '',
              )}
              onClick={() => goHear(item.id)}
            >
              <span style={{ fontSize: 30 - +item.level * 2 + 'px' }}>·</span>
              {item.text}
            </li>
          );
        })}
      </ul>
    </div>
  );
};
export default Linkage;
