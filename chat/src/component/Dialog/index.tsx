import React, { ReactElement, ReactNode, useState } from 'react';
import { createPortal } from 'react-dom';

interface Props {
  children: ReactNode;
  bol: boolean;
}

export default function index(props: Props): ReactElement {
  let html = (
    <div
      style={{
        background: 'rgba(0,0,0,.9)',
        width: '100%',
        height: '100%',
        position: 'fixed',
        top: '0',
        left: '0',
        paddingTop: '100px',
        zIndex: 999,
        display: props.bol ? "" : "none",
        overflowY: 'auto',
      }}
    >
      {props.children}
    </div>
  );
  return createPortal(html, document.querySelector('#root')!)

}
