import React, { ReactElement } from 'react'
import { Spin } from 'antd';
import Style from "./index.less"

interface Props {
}

const SPin:React.FC<Props>=(props)=>{
    return (
        <div className={Style.box}>
        <Spin
          size="large"
          delay={200}
        />
      </div>
    )
}

export default SPin;
