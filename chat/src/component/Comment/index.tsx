import React from "react";
import style from "./style.less"
import { IAegardsItem } from '@/types';
import Moment from "@/utils/moment";
import { MessageOutlined } from '@ant-design/icons';
import Popouts from "@/component/Popouts"

const Comment: React.FC<{ item: IAegardsItem, callback: (id: string, parentInd?: string) => void }> = ({ item, callback }: { item: IAegardsItem, callback: (id: string, parentInd?: string) => void }) => {
  return <div className={style.commentItem} key={item.id}>
    <div className={style.img}>
      <span style={{ backgroundColor: `rgba(${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)})` }}>{item.name.substr(0, 1).toUpperCase()}</span>
      <b>{item.name}</b>
    </div>
    <div className={style.commentText}>
      <div className={style.content} dangerouslySetInnerHTML={{ __html: item.html ? item.html : item.content }}></div>
      <p className={style.time}><span>{item.userAgent}</span>.<span>{Moment(item.createAt, "YYYYMMDD").fromNow()}</span><span onClick={() => callback(item.id, (item.parentCommentId || "0"))}><MessageOutlined />回复</span></p>
      <div style={{ width: "100%", height: "100%" }}>
        {!item.pass && <Popouts bol={true} callback={() => callback(item.id, (item.parentCommentId || "0"))} />}
      </div>
      {item.children && item.children.map((ite: any) => {
        return <Comment item={ite} callback={callback} key={ite.id} />
      })}
    </div>
  </div>
}
export default Comment;