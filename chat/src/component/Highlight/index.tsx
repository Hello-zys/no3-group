import React from "react"
import hljs from "highlight.js"
import { useEffect } from "react"
import { copyText } from "@/utils/copy"
import "./index.less"
const HighLight: React.FC = (props) => {
  const dom = React.createRef<HTMLDivElement>()
  useEffect(() => {
    //    识别
    let blocks = dom.current!.querySelectorAll("pre code");
    blocks.forEach(block => {
      hljs.highlightElement(block as HTMLElement);
      //    添加复制按钮
      let copyBtn = document.createElement("button")
      copyBtn.textContent = "复制"
      copyBtn.className = "copy-btn";
      block.parentNode!.insertBefore(copyBtn, block)
      //  点击复制按钮
      copyBtn.onclick = function () {
        copyText(block.textContent!)
      }
    })
  }, [props.children])
  return (
    <div ref={dom} className="markdown">{props.children}</div>
  )

}
export default HighLight;
