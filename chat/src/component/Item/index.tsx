import React from "react"
import styles from "./index.less"
import Moment from "@/utils/moment"
import { IArticleItem } from '@/types';
import { NavLink } from "umi"
import { EyeOutlined, ShareAltOutlined, HeartOutlined } from '@ant-design/icons';
import share from "@/component/Sharing"
interface Props {
  item: Partial<IArticleItem>,
}
const Item: React.FC<Props> = (props) => {
  function getShare(e: React.MouseEvent, item: Partial<IArticleItem>) {
    e.defaultPrevented = true
    e.stopPropagation();
    share(item)
  }

  return <div className={styles.context}>
    <li key={props.item.id}>
      <NavLink to={`/archive/${props.item.id}`} className={styles.contextTiTle}>
        <b className={styles.title}>{props.item.title}</b>
        <span>{Moment(props.item.createAt, "YYYYMMDD").fromNow()}</span>
        {props.item.category && <span>{props.item.category.label}</span>}
      </NavLink>
      <div className={styles.context_text}>
        {props.item.cover &&
          <NavLink to={`/archive/${props.item.id}`} className={styles.img}>
            <img src={props.item.cover} alt="" />
          </NavLink>
        }
        <div className={styles.text}>
          <NavLink to={`/archive/${props.item.id}`} className={styles.text_title}>{props.item.summary}</NavLink>
          <p className={styles.geng}>
            {props.item.likes ?<NavLink to={`/archive/${props.item.id}`}><HeartOutlined />{props.item.likes} <b>·</b></NavLink> :""}
            <NavLink to={`/archive/${props.item.id}`}><EyeOutlined />{props.item.views}</NavLink>
            <b>·</b>
            <span onClick={(e) => getShare(e, props.item)}><ShareAltOutlined />分享</span>
          </p>
        </div>
      </div>
    </li>
  </div>
}

export default Item