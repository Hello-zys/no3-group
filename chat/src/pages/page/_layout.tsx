import style from './layout.less';
import React, { useEffect, useState } from 'react';
import { IRootState, IRegardsState } from '@/types';
import Item from '@/component/Item';
import { useSelector, useDispatch } from 'umi';
import { Pagination } from 'antd';
import Popouts from "@/component/Popouts";
import Comment from "@/component/Comment"

export default function IndexPage(props: any) {
  const [page, setPage] = useState(1)
  const { regardsList, regardsListCount, } = useSelector((state: IRegardsState) => state.regards);
  const { recommend } = useSelector((state: IRootState) => state.article)
  const dispatch = useDispatch();
  const dom = React.createRef<HTMLDivElement>()
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
  }, [])

  function onChange(current: number) {
    setPage(page => page = current);
    dom.current?.scrollIntoView({
      behavior: "smooth",
      block: "start"
    });
  }
  // 切换pass的
  const changePass = (id: string, parentInd: string = "0") => {
    console.log(id, "我点击了");
    dispatch({
      type: "regards/changeregardsList",
      payload: {
        id,
        parentInd
      }
    })
  }
  return (
    <div>
      <div>{props.children}</div>
      <div className={style.commentContext}>
        <div className={style.comment}>
          <div className={style.aa1}>
            <p> 评论</p>
          </div>
          <div className={style.topbox}><Popouts bol={false} callback={() => { }}></Popouts></div>
          <div className={style.commentList} ref={dom}>
            {
              regardsList.map(item => {
                return <Comment item={item} callback={changePass} key={item.id} />
              })
            }
            <div className={style.pagination}>
              <Pagination defaultCurrent={1} total={regardsListCount} pageSize={6} current={page}
                onChange={onChange} />
            </div>
          </div>
          <div className={style.xm}>
            <p className={style.p1}>推荐阅读</p>
            {
              recommend.map(item => {
                return <Item key={item.id} item={item}></Item>
              })
            }
          </div>
        </div>
      </div>
    </div>
  );
}