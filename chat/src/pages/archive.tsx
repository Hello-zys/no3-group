import React, { useEffect } from 'react';
import styles from './archive.less';
import { NavLink, useDispatch, useSelector } from 'umi';
import { Gostate, IArchiveStateItem, IRootState, July2 } from '@/types';
import Moment from '@/utils/moment';
import { RouteComponentProps } from "react-router-dom"
// 引入分类组件
import ArchiveRecommend from '../component/RecommendedReading/index';
import ArticalSort from '../component/ArticleClassification';

const Archive: React.FC<RouteComponentProps> = (props) => {
  const dispatch = useDispatch();
  const { archiveList, CategoryList } = useSelector(
    (state: IArchiveStateItem) => state.archive,
  );
  const state = useSelector((state: IRootState) => ({ ...state.article, ...state.loading.effects }));
  useEffect(() => {
    dispatch({
      type: 'archive/getArchiveList',
    });
  }, []);

  // 推荐数据
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
    });
  }, []);
  //二级文章数据
  useEffect(() => {
    dispatch({
      type: 'archive/getCategory',
    });
  }, []);
  return (
    <div className={styles._box_box}>
      <div className={styles.container}>
        {/* 左边 */}
        <div className={styles.left}>
          <div className={styles.title}>
            <div className={styles.Item_title}>
              <p>
                <span className={styles.titlename}>归档</span>
              </p>
              <p>
                总计<span className={styles.conut}>33</span>篇
              </p>
            </div>
            <div className={styles.Item_All}>
              <div className={styles.Item_top}>
                <h2>{Object.keys(archiveList)[1]}</h2>
                {archiveList[2020] &&
                  Object.keys(archiveList[2021]).map((item, index) => {
                    return (
                      <div key={index} className={styles.MouthValue}>
                        <h3>{item}</h3>
                        <ul>
                          {archiveList[2021][item].map((item2: July2) => {
                            return (
                              <li className={styles.active} key={item2.id}>
                                <NavLink
                                  to={`/archive/${item2.id}`}
                                  className={styles.nav}
                                >
                                  <p>
                                    <span className={styles.timer}>
                                      <time
                                        dateTime={Moment(
                                          item2.publishAt,
                                        ).format('MM-DD')}
                                      >
                                        {Moment(item2.publishAt).format(
                                          'MM-DD',
                                        )}
                                      </time>
                                    </span>
                                    <span className={styles.spans}>
                                      {item2.title}
                                    </span>
                                  </p>
                                </NavLink>
                              </li>
                            );
                          })}
                        </ul>
                      </div>
                    );
                  })}
              </div>
              <div className={styles.Item_bot}>
                <h2>{Object.keys(archiveList)[0]}</h2>

                {archiveList[2020] &&
                  Object.keys(archiveList[2020]).map((item, index) => {
                    return (
                      <div key={index} className={styles.MouthValue}>
                        <h3>{item}</h3>
                        <ul key={index}>
                          {/* onClick={()=>this.changeDetail(item2.id)} */}
                          {archiveList[2020][item].map((item2: July2) => {
                            return (
                              <li className={styles.active} key={item2.id}>
                                <NavLink
                                  to={`/archive/${item2.id}`}
                                  className={styles.nav}
                                >
                                  <p>
                                    <span style={{ margin: '0 5px' }}>
                                      <time
                                        dateTime={Moment(
                                          item2.publishAt,
                                        ).format('MM-DD')}
                                      >
                                        {Moment(item2.publishAt).format(
                                          'MM-DD',
                                        )}
                                      </time>
                                    </span>
                                    <span className={styles.spans}>
                                      {item2.title}
                                    </span>
                                  </p>
                                </NavLink>
                              </li>
                            );
                          })}
                        </ul>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        {/* 右边 */}
        <div className={styles.right}>
          <ArchiveRecommend recommend={state.recommend} bol={state['article/getRecommend'] && state.recommend.length === 0}></ArchiveRecommend>
          <ArticalSort CategoryList={CategoryList} callback={(id) => { props.history.push(`/category/${id}`) }} bol={state["archive/getCategory"]&&CategoryList.length===0}></ArticalSort>
        </div>
      </div>
    </div>
    // </div>
  );
};
export default Archive;
