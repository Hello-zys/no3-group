import React, { useEffect, useMemo, useState } from "react"
import Route from '@/component/Route'
import styles from "./index.less"
import Item from "@/component/Item"
import { useDispatch, useSelector, IRouteComponentProps } from "umi"
import { IRootState } from "@/types"
import RecommendedReading from "@/component/RecommendedReading"
import InfiniteScroll from 'react-infinite-scroll-component';
import Lable from '@/component/lable';

const Fe: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  let [page, setPage] = useState(1);
  const id = props.match.url;
  const state = useSelector((state: IRootState) => ({ ...state.article, ...state.loading.effects }))
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch({
      type: 'article/getUpdataFelist',
      payload: { page, id },
    });
    dispatch({
      type: 'article/getFeListName',
      payload: id,
    });
  }, [id]);
  useEffect(() => {
    dispatch({
      type: 'article/getFeList',
      payload: { page, id },
    });
  }, [page]);
  useEffect(() => {
    dispatch({
      type: "article/getArticlelabel"
    })
  }, [])

  function pullupLoader() {
    setPage(page => page + 1)
  }
  return <div className={styles.wrap}>
    <div className={styles.container}>
      <div className={styles.context}>
        <div className={styles.left}>
          <div className={styles.swiper}>
            <div className={styles.img}>
              <p><span className={styles.text}>{state.feListName}</span>分类文章</p>
              <p>共搜索到<span className={styles.count}>{state.feListCount}</span>篇</p>
            </div>
          </div>
          {/* 路由按钮 */}
          <Route></Route>
          {/* 列表内容 */}
          <div style={{ overflow: "auto" }}>
            <InfiniteScroll
              hasMore={state.feListCount > page * 12}
              scrollableTarget="scrollableDiv"
              loader={<h4>Loading...</h4>}
              dataLength={state.feList.length}
              next={pullupLoader}
            >{
                state.feList.map(item => {
                  return <Item key={item.id} item={item}></Item>
                })
              }</InfiniteScroll>
          </div>
        </div>
        <div className={styles.right}>
          {/* 推荐阅读 */}
          <RecommendedReading
            bol={state["article/getRecommend"] && state.recommend.length === 0}
            recommend={state.recommend}
          ></RecommendedReading>
          {/* 文章标签 */}
          <Lable
            bol={state["article/getArticlelabel"] && state.articlelableList.length === 0}
            articlelableList={state.articlelableList}
          ></Lable>
        </div>
      </div>
    </div>
  </div >


}

export default Fe