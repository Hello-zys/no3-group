import { RouteComponentProps, useHistory } from 'react-router-dom';
import { IndexModelState } from '@/models/git';
import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, memo, useCallback } from 'react';
import Style from './tag.less';
import Labels from '@/component/lable';
import Articals from '@/component/Item';
import Recommends from '@/component/RecommendedReading';
import Readings from '@/component/ArticleClassification';

const Label = memo(Labels);
const Artical = memo(Articals);
const Recommend = memo(Recommends);
const Reading = memo(Readings);

interface IGit {
  'article/getArticlelabel': boolean;
  'git/getGitTag': boolean;
  'git/getGitRecomment': boolean;
  'git/getGitDetail': boolean;
  'git/getGitArtical': boolean;
}

const Git: React.FC<RouteComponentProps<{ tag: string }>> = (props) => {
  // 用仓库数据
  const state = useSelector(
    (state: {
      git: IndexModelState;
      loading: { effects: IGit; global: boolean };
    }) => ({
      ...state.git,
      ...state.loading.effects,
      global: state.loading.global,
    }),
  );
  // 获取数据
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: 'git/getGitTag',
    });
    dispatch({
      type: 'git/getGitRecomment',
    });
    dispatch({
      type: 'git/getGitDetail',
      payload: {
        tag: props.match.params.tag,
      },
    });
    dispatch({
      type: 'git/getGitArtical',
    });
    console.log(state);
  }, [props.match.params.tag]);

  const history = useHistory();

  return (
    <div className={Style.containe}>
      <div className={Style.left}>
        <div className={Style.ltop}>
          <h2>
            与<span className={Style.red}>{props.match.params.tag}</span>
            标签有关的文章
          </h2>
          <h3>
            共搜到<span className={Style.red}>{state.detail[1]}</span>篇
          </h3>
        </div>
        <div className={Style.main}>
          <Label
            bol={state['git/getGitTag'] && state.tag.length === 0}
            articlelableList={state.tag}
          />
        </div>
        <div className={Style.bottom}>
          {state.detail[0]?.map((item) => {
            return <Artical item={item} key={item.id} />;
          })}
        </div>
      </div>
      <div className={Style.right}>
        <div className="top">
          {state.recommend && (
            <Recommend
              recommend={state.recommend}
              bol={state['git/getGitRecomment'] && state.recommend.length === 0}
            />
          )}
        </div>
        <div className="bottom">
          {state.tag && (
            <Reading
              callback={useCallback((id) => {
                history?.push(`/git/${id}`);
              }, [])}
              CategoryList={state.tag}
              bol={state['git/getGitTag'] && state.tag.length === 0}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default Git;
