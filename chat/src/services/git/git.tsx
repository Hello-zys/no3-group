import { request } from 'umi';

//右侧推荐阅读
export const getGitRecomment = () => {
  return request("/api/article/recommend")
}

// 下侧文章
export const getGitDetail = (tag: string, page = 1, pageSize = 12, status = "publish") => {
  return request("/api/article/tag/" + tag, {
    params: {
      page,
      pageSize,
      status
    }
  })
}

// 获取标签
export const getGitTag = (articalStatus = "publish") => {
  return request("/api/tag", {
    params: {
      articalStatus
    }
  })
}

// 右侧文章分类
export const getGitArtical = (articleStatus = "publish") => {
  return request("/api/category", {
    params: {
      articleStatus
    }
  })
}