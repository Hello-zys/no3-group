import { request } from 'umi';
import { IPostItem } from '@/types';

// 获取文章列表
export function getArticleList(page: number, pageSize = 12, status = 'publish') {
  return request('/api/article', {
    params: {
      page,
      pageSize,
      status,
    }
  })
}
// 获取文章标签
export function getArticlelabel(articleStatus = "publish") {
  return request("/api/tag", {
    params: {
      articleStatus
    }
  })
}
// 获取推荐文章
export function getRecommend() {
  return request('/api/article/recommend')
}
// 获取前端列表
export function getFeList(page: number, id: string, pageSize = 12, status = "publish") {
  return request(`/api/article${id}`, {
    params: {
      page,
      pageSize,
      status
    }
  })
}

// api/category/be
export function getFeListName(id: string,) {
  return request(`/api${id}`)
}

// 生成海报
export function genePoster(data: IPostItem) {
  return request(`/api/poster`, {
    method: 'POST',
    data
  })
}

