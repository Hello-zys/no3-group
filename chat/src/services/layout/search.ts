import { request } from "umi"
export function getSearch(text: string) {
  return request("/api/search/article", {
    params: {
      keyword: text
    }
  })
}