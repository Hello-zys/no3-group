import {mylist} from '../../services/modules/my'

const state = {
    list: []
};

const getters = {};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async myList({commit},payload){
        // console.log();
        let result=await mylist()
        console.log(result.data,'11222222222222222222222222222');

        if(result.errNo === 0)
        {
            commit('update',{
                list:result.data,
                // console.log(list,'44444444444')
            })
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}