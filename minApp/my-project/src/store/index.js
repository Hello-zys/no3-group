import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger';
// 引入子模块
import index from './modules/index'
import bigbrand from './modules/bigbrand'
import my from './modules/my';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        index,
        bigbrand,
        my
    },
    plugins: [createLogger()]
});